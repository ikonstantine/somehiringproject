package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.dto.request.ExitPersonDtoRequest;
import net.some.kruglov.hiring.dto.request.RegisterEmployeeDtoRequest;
import net.some.kruglov.hiring.server.Server;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestPersonOutService {
    @Test
    public void testPersonExit() {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        ExitPersonDtoRequest exitPersonRequest = new ExitPersonDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2), true);
        String jsonRequest2 = new Gson().toJson(exitPersonRequest);
        String jsonResponse2 = server.exitFromServer(jsonRequest2);

        assertEquals(jsonResponse2, "{}");
    }
}
