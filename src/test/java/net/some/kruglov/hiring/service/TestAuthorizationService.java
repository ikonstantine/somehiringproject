package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBaseErrorCode;
import net.some.kruglov.hiring.validator.ValidatorErrorCode;
import net.some.kruglov.hiring.dto.request.AuthorizationPersonDtoRequest;
import net.some.kruglov.hiring.dto.request.RegisterEmployeeDtoRequest;
import net.some.kruglov.hiring.server.Server;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestAuthorizationService {
    @Test
    public void testSuccessAuthorise() {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "mylogin1996", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonRegTokenResponse = server.registerEmployee(jsonRequest1);

        AuthorizationPersonDtoRequest authorizationPersonDtoRequest = new AuthorizationPersonDtoRequest("mylogin1996",
                "passWord1234%");
        String jsonRequest2 = new Gson().toJson(authorizationPersonDtoRequest);
        String jsonAuthTokenResponse = server.authorization(jsonRequest2);

        assertEquals(jsonRegTokenResponse, jsonAuthTokenResponse);
    }

    @Test
    public void testFailAuthoriseByNotExistLogin() {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "anotherlogin", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonRegTokenResponse = server.registerEmployee(jsonRequest1);

        AuthorizationPersonDtoRequest authorizationPersonDtoRequest = new AuthorizationPersonDtoRequest("mylogin1996",
                "passWord1234%");
        String jsonRequest2 = new Gson().toJson(authorizationPersonDtoRequest);
        String jsonAuthTokenResponse = server.authorization(jsonRequest2);

        assertTrue(jsonAuthTokenResponse.contains(DataBaseErrorCode.DATA_BASE_LOGIN_NOT_EXIST.toString()));
    }

    @Test
    public void testFailAuthoriseByWrongPassword() {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "mylogin1996", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonRegTokenResponse = server.registerEmployee(jsonRequest1);

        AuthorizationPersonDtoRequest authorizationPersonDtoRequest = new AuthorizationPersonDtoRequest("mylogin1996",
                "pasHjd^78g%fs");
        String jsonRequest2 = new Gson().toJson(authorizationPersonDtoRequest);
        String jsonAuthTokenResponse = server.authorization(jsonRequest2);

        assertTrue(jsonAuthTokenResponse.contains(ValidatorErrorCode.WRONG_PASSWORD.toString()));
    }
}
