package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseErrorCode;
import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.dto.request.*;
import net.some.kruglov.hiring.validator.ValidatorErrorCode;
import net.some.kruglov.hiring.server.Server;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestEmployeeService {
    @Test
    public void testSuccessfulRegistrationEmployee() {
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "imconstantine", "passWord1996%");
        String jsonRequest = new Gson().toJson(employeeDtoRequest);
        String jsonResponse = new EmployeeService(new DataBase()).registerEmployee(jsonRequest);
        assertTrue(jsonResponse.contains("token"));
    }

    @Test
    public void testFailRegistraionEmployeeByWrongPassword() {
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "imconstantine", "4g4egd");
        String jsonRequest = new Gson().toJson(employeeDtoRequest);
        String jsonResponse = new EmployeeService(new DataBase()).registerEmployee(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_PASSWORD.toString()));
    }

    @Test
    public void testFailRegistraionEmployeeByWrongEmail() {
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantineya.ru", "imconstantine", "passWord1234%");
        String jsonRequest = new Gson().toJson(employeeDtoRequest);
        String jsonResponse = new EmployeeService(new DataBase()).registerEmployee(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_EMAIL.toString()));
    }

    @Test
    public void testFailRegistraionEmployeeByWrongName() {
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("s",
                "ikonstantine@ya.ru", "imconstantine", "passWord1234%");
        String jsonRequest = new Gson().toJson(employeeDtoRequest);
        String jsonResponse = new EmployeeService(new DataBase()).registerEmployee(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_NAME.toString()));
    }

    @Test
    public void testFailRegistraionEmployeeByWrongLogin() {
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "", "passWord1234%");
        String jsonRequest = new Gson().toJson(employeeDtoRequest);
        String jsonResponse = new EmployeeService(new DataBase()).registerEmployee(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_LOGIN.toString()));
    }

    @Test
    public void testFailRegistraionEmployeeByExistingLogin() {
        RegisterEmployeeDtoRequest employeeDtoRequest1 = new RegisterEmployeeDtoRequest("Kostya N",
                "ikonstantine@ya.ru", "javapromegasuper", "passWord1234%");
        RegisterEmployeeDtoRequest employeeDtoRequest2 = new RegisterEmployeeDtoRequest("Alex N",
                "ialex1235@ya.ru", "javapromegasuper", "passWord1234%");

        String jsonRequest1 = new Gson().toJson(employeeDtoRequest1);
        String jsonRequest2 = new Gson().toJson(employeeDtoRequest2);

        DataBase dataBase = new DataBase();
        EmployeeService employeeService = new EmployeeService(dataBase);
        employeeService.registerEmployee(jsonRequest1);
        String jsonResponse2 = employeeService.registerEmployee(jsonRequest2);

        assertTrue(jsonResponse2.contains("error"));
        assertTrue(jsonResponse2.contains(DataBaseErrorCode.DATA_BASE_LOGIN_ALREADY_EXIST.toString()));
    }

    @Test
    public void testLeaveEmployeeFromServer() {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        ExitPersonDtoRequest exitPersonRequest = new ExitPersonDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2), true);
        String jsonRequest2 = new Gson().toJson(exitPersonRequest);
        String jsonResponse2 = server.leaveFromServerByEmployee(jsonRequest2);

        assertEquals(jsonResponse2, "{}");
    }

    @Test
    public void testEmployeeAddSkills() {
        Server server = new Server();
        server.start();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("Spring", 4, null));
        skills.add(new RequirementDto("Apache", 4, null));
        SkillsDtoRequest skillsDtoRequest = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);
        String jsonRequest2 = new Gson().toJson(skillsDtoRequest);
        String jsonResponse2 = server.addSkills(jsonRequest2);
        assertEquals(jsonResponse2, "{}");
    }

    @Test
    public void testEmployeeFailAddSkillsByExistSkill() {
        Server server = new Server();
        server.start();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);
        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        SkillsDtoRequest skillsDtoRequest2 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);
        String jsonRequest3 = new Gson().toJson(skillsDtoRequest2);
        String jsonResponse3 = server.addSkills(jsonRequest2);
        assertTrue(jsonResponse3.contains(PersonErrorCode.DUPLICATE_SKILL.toString()));
    }

    @Test
    public void testEmployeeFailAddSkillsByWrongLevel() {
        Server server = new Server();
        server.start();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 6, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);
        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);
        assertTrue(jsonResponse2.contains(PersonErrorCode.SKILL_WRONG_LEVEL.toString()));
    }

    @Test
    public void testEmployeeEditData() {
        Server server = new Server();
        server.start();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("Apache", 5, null));
        skills.add(new RequirementDto("English", 4, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        Map<String, RequirementDto> map = new HashMap<>();
        map.put("Java", new RequirementDto("Java SE", 4, null));
        map.put("Apache", new RequirementDto("DEL_SKILL", 4, null));
        map.put("ADD_SKILL", new RequirementDto("Ruby on Rails", 5, null));

        ChangeEmployeeDataDtoRequest ceddr =
                new ChangeEmployeeDataDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        "Konstantin Kruglov", "newforworkemail@work.com", null, false, map);
        String jsonRequest3 = new Gson().toJson(ceddr);
        String jsonResponse3 = server.editEmployeeData(jsonRequest3);

        assertEquals(jsonResponse3, "{}");
    }

    @Test
    public void testEmployeeFailEditDataByWrongEmail() {
        Server server = new Server();
        server.start();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("Apache", 5, null));
        skills.add(new RequirementDto("English", 4, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        Map<String, RequirementDto> map = new HashMap<>();
        map.put("Java", new RequirementDto("Java SE", 4, null));
        map.put("Apache", new RequirementDto("DEL_SKILL", 4, null));
        map.put("ADD_SKILL", new RequirementDto("Ruby on Rails", 5, null));

        ChangeEmployeeDataDtoRequest ceddr =
                new ChangeEmployeeDataDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        "Konstantin Kruglov", "newforworkemailwork.com", null, false, map);
        String jsonRequest3 = new Gson().toJson(ceddr);
        String jsonResponse3 = server.editEmployeeData(jsonRequest3);

        assertTrue(jsonResponse3.contains(ValidatorErrorCode.NOT_VALIDATE_EMAIL.toString()));
    }

    @Test
    public void testEmployeeFailEditDataByAddExistSkill() {
        Server server = new Server();
        server.start();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Valid Name",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("Apache", 5, null));
        skills.add(new RequirementDto("English", 4, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        Map<String, RequirementDto> map = new HashMap<>();
        map.put("Java", new RequirementDto("Java SE", 4, null));
        map.put("Apache", new RequirementDto("DEL_SKILL", 4, null));
        map.put("ADD_SKILL", new RequirementDto("English", 5, null));

        ChangeEmployeeDataDtoRequest ceddr =
                new ChangeEmployeeDataDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        "Konstantin Kruglov", "newforworkemail@work.com", null, false, map);
        String jsonRequest3 = new Gson().toJson(ceddr);
        String jsonResponse3 = server.editEmployeeData(jsonRequest3);
        assertTrue(jsonResponse3.contains(PersonErrorCode.DUPLICATE_SKILL.toString()));
    }
}
