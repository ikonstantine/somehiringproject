package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseErrorCode;
import net.some.kruglov.hiring.dto.request.*;
import net.some.kruglov.hiring.validator.ValidatorErrorCode;
import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.server.Server;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestEmployerService {
    @Test
    public void testSuccessfulRegistrationEmployer() {
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "imconstantine", "passWord1996%", "Dunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest = new Gson().toJson(employerDtoRequest);
        String jsonResponse = new EmployerService(new DataBase()).registerEmployer(jsonRequest);
        assertTrue(jsonResponse.contains("token"));
    }

    @Test
    public void testFailRegistraionEmployerByWrongPassword() {
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "imconstantine", "", "Dunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest = new Gson().toJson(employerDtoRequest);
        String jsonResponse = new EmployerService(new DataBase()).registerEmployer(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_PASSWORD.toString()));
    }
    @Test
    public void testFailRegistraionEmployerByWrongName() {
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("a",
                "ikonstantine@ya.ru", "imconstantine", "Qazwsx123^&", "Dunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest = new Gson().toJson(employerDtoRequest);
        String jsonResponse = new EmployerService(new DataBase()).registerEmployer(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_NAME.toString()));
    }
    @Test
    public void testFailRegistraionEmployerByWrongEmail() {
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantineya.ru", "imconstantine", "Qazwsx123^&", "Dunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest = new Gson().toJson(employerDtoRequest);
        String jsonResponse = new EmployerService(new DataBase()).registerEmployer(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_EMAIL.toString()));
    }
    @Test
    public void testFailRegistraionEmployerByWrongLogin() {
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "imco", "", "Dunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest = new Gson().toJson(employerDtoRequest);
        String jsonResponse = new EmployerService(new DataBase()).registerEmployer(jsonRequest);
        assertTrue(jsonResponse.contains("error"));
        assertTrue(jsonResponse.contains(ValidatorErrorCode.NOT_VALIDATE_LOGIN.toString()));
    }

    @Test
    public void testFailRegistraionEmployerByExistingLogin() {
        RegisterEmployerDtoRequest employerDtoRequest1 = new RegisterEmployerDtoRequest("Petr Alexeevich Kruglov",
                "forwork@outitcompany.ru", "megasuperitcompany", "ValidP@s123%", "Dunder mifflin inc.", "lenina 3");
        RegisterEmployerDtoRequest employerDtoRequest2 = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");

        String jsonRequest1 = new Gson().toJson(employerDtoRequest1);
        String jsonRequest2 = new Gson().toJson(employerDtoRequest2);

        DataBase dataBase = new DataBase();
        EmployerService employerService = new EmployerService(dataBase);
        String jsonResponse1 = employerService.registerEmployer(jsonRequest1);
        String jsonResponse2 = employerService.registerEmployer(jsonRequest2);

        assertTrue(jsonResponse2.contains("error"));
        assertTrue(jsonResponse2.contains(DataBaseErrorCode.DATA_BASE_LOGIN_ALREADY_EXIST.toString()));
    }

    @Test
    public void testLeaveEmployeeFromServer() {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        ExitPersonDtoRequest exitPersonRequest = new ExitPersonDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2), true);
        String jsonRequest2 = new Gson().toJson(exitPersonRequest);
        String jsonResponse2 = server.leaveFromServerByEmployer(jsonRequest2);

        assertEquals(jsonResponse2, "{}");
    }

    @Test
    public void testAddVacancy() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("English", 4, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest);

        String jsonResponse2 = server.addVacancy(jsonRequest2);
        assertTrue(jsonResponse2.contains("{}"));
    }

    @Test
    public void testFailAddVacancyByWrongName() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("XML", 4, true));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest);

        String jsonResponse2 = server.addVacancy(jsonRequest2);
        assertTrue(jsonResponse2.contains(PersonErrorCode.VACANCY_WRONG_NAME.toString()));
    }

    @Test
    public void testFailAddVacancyByWrongSalary() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("XML", 4, true));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Ruby developer", -1, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest);

        String jsonResponse2 = server.addVacancy(jsonRequest2);
        assertTrue(jsonResponse2.contains(PersonErrorCode.NEGATIVE_SALARY.toString()));
    }

    @Test
    public void testFailAddVacancyByWrongRequirementByWrongLevel() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("English", 5, true));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest).replace(":5", ":-1");

        String jsonResponse2 = server.addVacancy(jsonRequest2);
        assertTrue(jsonResponse2.contains(PersonErrorCode.SKILL_WRONG_LEVEL.toString()));
    }

    @Test
    public void testFailAddVacancyByWrongRequirementByWrongSkillName() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("some Skill", 3, true));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest).replace("some Skill", "");

        String jsonResponse2 = server.addVacancy(jsonRequest2);
        assertTrue(jsonResponse2.contains(PersonErrorCode.SKILL_WRONG_NAME.toString()));
    }

    @Test
    public void testEditEmployerData() {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        ChangeEmployerDataDtoRequest employerDataDtoRequest =
                new ChangeEmployerDataDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        null, "forwork@company.com", null, null, null);
        String jsonRequest2 = new Gson().toJson(employerDataDtoRequest);
        String jsonResponse2 = server.editEmployerData(jsonRequest2);

        assertEquals(jsonResponse2, "{}");
    }

    @Test
    public void testFailEditEmployerData() {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        ChangeEmployerDataDtoRequest employerDataDtoRequest =
                new ChangeEmployerDataDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        null, null, null, null, null);
        String jsonRequest2 = new Gson().toJson(employerDataDtoRequest);
        String jsonResponse2 = server.editEmployerData(jsonRequest2);

        assertTrue(jsonResponse2.contains(ValidatorErrorCode.EMPLOYER_NOTHING_TO_EDIT.toString()));
    }

    @Test
    public void testSuccessEditVacancy() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("English", 4, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest);
        String jsonResponse2 = server.addVacancy(jsonRequest2);

        Map<String, RequirementDto> map = new HashMap<>();
        map.put("XML", new RequirementDto("DEL_REQ", 1, false));
        map.put("Apache", new RequirementDto(null, null, false));
        map.put("ADD_REQ", new RequirementDto("Java", 5, true));
        ChangeVacancyDtoRequest changeVacancyDtoRequest =
                new ChangeVacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        "Junior Java Developer", null, 101, null, map);
        String jsonRequest3 = new Gson().toJson(changeVacancyDtoRequest);
        String jsonResponse3 = server.editVacancyData(jsonRequest3);
        assertEquals(jsonResponse3, "{}");
    }

    @Test
    public void testSuccessDeleteVacancy() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("English", 4, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest);
        String jsonResponse2 = server.addVacancy(jsonRequest2);

        Map<String, RequirementDto> map = new HashMap<>();
        map.put("XML", new RequirementDto("DEL_REQ", 1, false));
        map.put("Apache", new RequirementDto(null, null, false));
        map.put("ADD_REQ", new RequirementDto("Java", 5, true));
        ChangeVacancyDtoRequest changeVacancyDtoRequest =
                new ChangeVacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        "Junior Java Developer", "DEL_VACANCY", 101, null, map);
        String jsonRequest3 = new Gson().toJson(changeVacancyDtoRequest);
        String jsonResponse3 = server.editVacancyData(jsonRequest3);
        assertEquals(jsonResponse3, "{}");
    }

    @Test
    public void testFailEditVacancyByWrongRequirementLevel() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Konstantin Alexeevich Kruglov",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest1 = new Gson().toJson(employerDtoRequest);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("English", 4, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest2 = new Gson().toJson(vacancyRequest);
        String jsonResponse2 = server.addVacancy(jsonRequest2);

        Map<String, RequirementDto> map = new HashMap<>();
        map.put("XML", new RequirementDto("DEL_REQ", 1, false));
        map.put("Apache", new RequirementDto(null, null, false));
        map.put("ADD_REQ", new RequirementDto("Java", 99999, true));
        ChangeVacancyDtoRequest changeVacancyDtoRequest =
                new ChangeVacancyDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                        "Junior Java Developer", "Senior Java Developer", 101, null, map);
        String jsonRequest3 = new Gson().toJson(changeVacancyDtoRequest);
        String jsonResponse3 = server.editVacancyData(jsonRequest3);
        assertTrue(jsonResponse3.contains(PersonErrorCode.SKILL_WRONG_LEVEL.toString()));
    }
}
