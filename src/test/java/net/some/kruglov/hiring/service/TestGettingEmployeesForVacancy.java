package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import net.some.kruglov.hiring.dto.request.*;
import net.some.kruglov.hiring.server.Server;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestGettingEmployeesForVacancy {
    @Test
    public void testGettingEmployeesWithAllMatchingRequirements() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest1 = new RegisterEmployeeDtoRequest("Konstantin Kruglov",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills1 = new ArrayList<>();
        skills1.add(new RequirementDto("Java", 5, null));
        skills1.add(new RequirementDto("Apache", 5, null));
        skills1.add(new RequirementDto("English", 5, null));
        skills1.add(new RequirementDto("XML", 5, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills1);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        RegisterEmployeeDtoRequest employeeDtoRequest2 = new RegisterEmployeeDtoRequest("Nekonstantin Kruglov",
                "alalala@mail.ru", "login2", "passWord1234%");
        String jsonRequest3 = new Gson().toJson(employeeDtoRequest2);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);

        List<RequirementDto> skills2 = new ArrayList<>();
        skills2.add(new RequirementDto("Ruby", 5, null));
        skills2.add(new RequirementDto("Apache", 5, null));
        skills2.add(new RequirementDto("English", 5, null));
        skills2.add(new RequirementDto("XML", 3, null));
        SkillsDtoRequest skillsDtoRequest2 = new SkillsDtoRequest(jsonResponse3.substring(10, jsonResponse3.length() - 2),
                skills2);

        String jsonRequest4 = new Gson().toJson(skillsDtoRequest2);
        server.addSkills(jsonRequest4);

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Some name",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest5 = new Gson().toJson(employerDtoRequest);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("English", 5, false));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest6 = new Gson().toJson(vacancyRequest);

        String jsonResponse6 = server.addVacancy(jsonRequest6);

        List<Requirement> requirements2 = new ArrayList<>();
        requirements2.add(new Requirement("Ruby", 5, true));
        requirements2.add(new Requirement("English", 5, false));
        requirements2.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest2 = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Ruby On Rails Developer", 80, requirements2);
        String jsonRequest8 = new Gson().toJson(vacancyRequest2);

        String jsonResponse8 = server.addVacancy(jsonRequest8);

        GetEmployeesForEachVacancyDtoRequest getEmployeesForEachVacancyDtoRequest =
                new GetEmployeesForEachVacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2));
        String jsonRequest7 = new Gson().toJson(getEmployeesForEachVacancyDtoRequest);
        String jsonResponse7 = server.getEmployeesForEachVacancy(jsonRequest7);

        assertTrue(jsonResponse7.contains("Junior Java Developer"));
        assertTrue(jsonResponse7.contains("Ruby On Rails Developer"));
        assertTrue(jsonResponse7.contains("Konstantin Kruglov"));
        assertTrue(jsonResponse7.contains("Nekonstantin Kruglov"));
    }

    @Test
    public void testGettingEmployeesWithNecessaryRequirements() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest1 = new RegisterEmployeeDtoRequest("Konstantin Kruglov",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills1 = new ArrayList<>();
        skills1.add(new RequirementDto("Java", 5, null));
        skills1.add(new RequirementDto("English", 5, null));
        skills1.add(new RequirementDto("XML", 5, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills1);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        RegisterEmployeeDtoRequest employeeDtoRequest2 = new RegisterEmployeeDtoRequest("Nekonstantin Kruglov",
                "alalala@mail.ru", "login2", "passWord1234%");
        String jsonRequest3 = new Gson().toJson(employeeDtoRequest2);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);

        List<RequirementDto> skills2 = new ArrayList<>();
        skills2.add(new RequirementDto("Ruby", 5, null));
        skills2.add(new RequirementDto("Apache", 5, null));
        skills2.add(new RequirementDto("English", 5, null));
        SkillsDtoRequest skillsDtoRequest2 = new SkillsDtoRequest(jsonResponse3.substring(10, jsonResponse3.length() - 2),
                skills2);

        String jsonRequest4 = new Gson().toJson(skillsDtoRequest2);
        server.addSkills(jsonRequest4);

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Some name",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest5 = new Gson().toJson(employerDtoRequest);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("English", 5, true));
        requirements.add(new Requirement("Apache", 5, false));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest6 = new Gson().toJson(vacancyRequest);

        String jsonResponse6 = server.addVacancy(jsonRequest6);

        List<Requirement> requirements2 = new ArrayList<>();
        requirements2.add(new Requirement("Ruby", 5, true));
        requirements2.add(new Requirement("English", 5, true));
        requirements2.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest2 = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Ruby On Rails Developer", 80, requirements2);
        String jsonRequest8 = new Gson().toJson(vacancyRequest2);

        String jsonResponse8 = server.addVacancy(jsonRequest8);

        GetEmployeesForEachVacancyDtoRequest getEmployeesForEachVacancyDtoRequest =
                new GetEmployeesForEachVacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2));
        String jsonRequest7 = new Gson().toJson(getEmployeesForEachVacancyDtoRequest);
        String jsonResponse7 = server.getEmployeesForEachVacancyForNecessaryRequirements(jsonRequest7);

        assertTrue(jsonResponse7.contains("Junior Java Developer"));
        assertTrue(jsonResponse7.contains("Ruby On Rails Developer"));
        assertTrue(jsonResponse7.contains("Konstantin Kruglov"));
        assertTrue(jsonResponse7.contains("Nekonstantin Kruglov"));
    }

    @Test
    public void testGettingEmployeesWithNecessaryRequirementsWithoutLevel() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest1 = new RegisterEmployeeDtoRequest("Konstantin Kruglov",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills1 = new ArrayList<>();
        skills1.add(new RequirementDto("Java", 1, null));
        skills1.add(new RequirementDto("English", 1, null));
        skills1.add(new RequirementDto("Apache", 1, null));
        skills1.add(new RequirementDto("XML", 1, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills1);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        RegisterEmployeeDtoRequest employeeDtoRequest2 = new RegisterEmployeeDtoRequest("Nekonstantin Kruglov",
                "alalala@mail.ru", "login2", "passWord1234%");
        String jsonRequest3 = new Gson().toJson(employeeDtoRequest2);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);

        List<RequirementDto> skills2 = new ArrayList<>();
        skills2.add(new RequirementDto("Ruby", 1, null));
        skills2.add(new RequirementDto("Apache", 1, null));
        skills2.add(new RequirementDto("English", 1, null));
        skills2.add(new RequirementDto("XML", 1, null));
        SkillsDtoRequest skillsDtoRequest2 = new SkillsDtoRequest(jsonResponse3.substring(10, jsonResponse3.length() - 2),
                skills2);

        String jsonRequest4 = new Gson().toJson(skillsDtoRequest2);
        server.addSkills(jsonRequest4);

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Some name",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest5 = new Gson().toJson(employerDtoRequest);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("English", 5, true));
        requirements.add(new Requirement("Apache", 5, false));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest6 = new Gson().toJson(vacancyRequest);

        String jsonResponse6 = server.addVacancy(jsonRequest6);

        List<Requirement> requirements2 = new ArrayList<>();
        requirements2.add(new Requirement("Ruby", 5, true));
        requirements2.add(new Requirement("English", 5, true));
        requirements2.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest2 = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Ruby On Rails Developer", 80, requirements2);
        String jsonRequest8 = new Gson().toJson(vacancyRequest2);

        String jsonResponse8 = server.addVacancy(jsonRequest8);

        GetEmployeesForEachVacancyDtoRequest getEmployeesForEachVacancyDtoRequest =
                new GetEmployeesForEachVacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2));
        String jsonRequest7 = new Gson().toJson(getEmployeesForEachVacancyDtoRequest);
        String jsonResponse7 = server.getEmployeesForEachVacancyForNecessaryRequirementWithOutLevel(jsonRequest7);

        assertTrue(jsonResponse7.contains("Junior Java Developer"));
        assertTrue(jsonResponse7.contains("Ruby On Rails Developer"));
        assertTrue(jsonResponse7.contains("Konstantin Kruglov"));
        assertTrue(jsonResponse7.contains("Nekonstantin Kruglov"));
    }

    @Test
    public void testGettingEmployeesAtLeastOneRequirement() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest1 = new RegisterEmployeeDtoRequest("Konstantin Kruglov",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills1 = new ArrayList<>();
        skills1.add(new RequirementDto("Java", 4, null));
        skills1.add(new RequirementDto("Russian lang", 1, null));
        skills1.add(new RequirementDto("Spring", 1, null));
        skills1.add(new RequirementDto("NoSQL", 1, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills1);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        RegisterEmployeeDtoRequest employeeDtoRequest2 = new RegisterEmployeeDtoRequest("Nekonstantin Kruglov",
                "alalala@mail.ru", "login2", "passWord1234%");
        String jsonRequest3 = new Gson().toJson(employeeDtoRequest2);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);

        List<RequirementDto> skills2 = new ArrayList<>();
        skills2.add(new RequirementDto("Scala", 4, null));
        skills2.add(new RequirementDto("Angular.js", 1, null));
        skills2.add(new RequirementDto("Node.js", 1, null));
        skills2.add(new RequirementDto("Python", 1, null));
        SkillsDtoRequest skillsDtoRequest2 = new SkillsDtoRequest(jsonResponse3.substring(10, jsonResponse3.length() - 2),
                skills2);

        String jsonRequest4 = new Gson().toJson(skillsDtoRequest2);
        server.addSkills(jsonRequest4);

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Some name",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest5 = new Gson().toJson(employerDtoRequest);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 4, true));
        requirements.add(new Requirement("English", 5, true));
        requirements.add(new Requirement("Apache", 5, false));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest6 = new Gson().toJson(vacancyRequest);

        String jsonResponse6 = server.addVacancy(jsonRequest6);

        List<Requirement> requirements2 = new ArrayList<>();
        requirements2.add(new Requirement("Ruby", 5, true));
        requirements2.add(new Requirement("English", 5, true));
        requirements2.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest2 = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Ruby On Rails Developer", 80, requirements2);
        String jsonRequest8 = new Gson().toJson(vacancyRequest2);

        String jsonResponse8 = server.addVacancy(jsonRequest8);

        GetEmployeesForEachVacancyDtoRequest getEmployeesForEachVacancyDtoRequest =
                new GetEmployeesForEachVacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2));
        String jsonRequest7 = new Gson().toJson(getEmployeesForEachVacancyDtoRequest);
        String jsonResponse7 = server.getEmployeesForEachVacancyAtLeastOneRequirement(jsonRequest7);

        assertTrue(jsonResponse7.contains("Junior Java Developer"));
        assertFalse(jsonResponse7.contains("Ruby On Rails Developer"));
        assertTrue(jsonResponse7.contains("Konstantin Kruglov"));
        assertFalse(jsonResponse7.contains("Nekonstantin Kruglov"));
    }

    @Test
    public void testGettingEmployeesAtLeastOneRequirementAndAcceptEmployee() throws PersonException {
        Server server = new Server();
        server.start();

        RegisterEmployeeDtoRequest employeeDtoRequest1 = new RegisterEmployeeDtoRequest("Konstantin Kruglov",
                "ikonstantine@ya.ru", "login", "passWord1234%");
        String jsonRequest1 = new Gson().toJson(employeeDtoRequest1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);

        List<RequirementDto> skills1 = new ArrayList<>();
        skills1.add(new RequirementDto("Java", 4, null));
        skills1.add(new RequirementDto("Russian lang", 1, null));
        skills1.add(new RequirementDto("Spring", 1, null));
        skills1.add(new RequirementDto("NoSQL", 1, null));
        SkillsDtoRequest skillsDtoRequest1 = new SkillsDtoRequest(jsonResponse1.substring(10, jsonResponse1.length() - 2),
                skills1);

        String jsonRequest2 = new Gson().toJson(skillsDtoRequest1);
        String jsonResponse2 = server.addSkills(jsonRequest2);

        RegisterEmployeeDtoRequest employeeDtoRequest2 = new RegisterEmployeeDtoRequest("Nekonstantin Kruglov",
                "alalala@mail.ru", "login2", "passWord1234%");
        String jsonRequest3 = new Gson().toJson(employeeDtoRequest2);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);

        List<RequirementDto> skills2 = new ArrayList<>();
        skills2.add(new RequirementDto("Scala", 4, null));
        skills2.add(new RequirementDto("Angular.js", 1, null));
        skills2.add(new RequirementDto("Node.js", 1, null));
        skills2.add(new RequirementDto("Python", 1, null));
        SkillsDtoRequest skillsDtoRequest2 = new SkillsDtoRequest(jsonResponse3.substring(10, jsonResponse3.length() - 2),
                skills2);

        String jsonRequest4 = new Gson().toJson(skillsDtoRequest2);
        server.addSkills(jsonRequest4);

        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Some name",
                "ikonstantine@ya.ru", "megasuperitcompany", "ValidP@s123%", "NeDunder mifflin inc.", "32423, himikhov 51");
        String jsonRequest5 = new Gson().toJson(employerDtoRequest);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);

        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 4, true));
        requirements.add(new Requirement("English", 5, true));
        requirements.add(new Requirement("Apache", 5, false));
        requirements.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Junior Java Developer", 100, requirements);
        String jsonRequest6 = new Gson().toJson(vacancyRequest);

        String jsonResponse6 = server.addVacancy(jsonRequest6);

        List<Requirement> requirements2 = new ArrayList<>();
        requirements2.add(new Requirement("Ruby", 5, true));
        requirements2.add(new Requirement("English", 5, true));
        requirements2.add(new Requirement("XML", 3, false));
        VacancyDtoRequest vacancyRequest2 = new VacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2),
                "Ruby On Rails Developer", 80, requirements2);
        String jsonRequest8 = new Gson().toJson(vacancyRequest2);

        String jsonResponse8 = server.addVacancy(jsonRequest8);

        GetEmployeesForEachVacancyDtoRequest getEmployeesForEachVacancyDtoRequest =
                new GetEmployeesForEachVacancyDtoRequest(jsonResponse5.substring(10, jsonResponse5.length() - 2));
        String jsonRequest7 = new Gson().toJson(getEmployeesForEachVacancyDtoRequest);
        String jsonResponse7 = server.getEmployeesForEachVacancyAtLeastOneRequirement(jsonRequest7);

        String employerToken = jsonResponse5.substring(10, jsonResponse5.length() - 2);
        String suitableEmployeeToken = jsonResponse7.substring(155, 191);
        AcceptEmployeeDtoRequest acceptRequest = new AcceptEmployeeDtoRequest("Junior Java Developer",
                jsonResponse5.substring(10, jsonResponse5.length() - 2), suitableEmployeeToken);
        String acceptResponse = server.acceptEmployee(new Gson().toJson(acceptRequest));

        assertEquals("{}", acceptResponse);
        assertTrue(jsonResponse7.contains("Junior Java Developer"));
        assertFalse(jsonResponse7.contains("Ruby On Rails Developer"));
        assertTrue(jsonResponse7.contains("Konstantin Kruglov"));
        assertFalse(jsonResponse7.contains("Nekonstantin Kruglov"));
    }
}
