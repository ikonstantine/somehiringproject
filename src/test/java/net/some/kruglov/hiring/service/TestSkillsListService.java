package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBaseErrorCode;
import net.some.kruglov.hiring.dto.request.AddSkillToSkillTitlesDtoRequest;
import net.some.kruglov.hiring.server.Server;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSkillsListService {

    @Test
    public void testAddingSkillToSkillsList() {
        Server server = new Server();
        server.start();

        AddSkillToSkillTitlesDtoRequest dtoRequest1 = new AddSkillToSkillTitlesDtoRequest("Java");
        AddSkillToSkillTitlesDtoRequest dtoRequest2 = new AddSkillToSkillTitlesDtoRequest("Apache");
        AddSkillToSkillTitlesDtoRequest dtoRequest3 = new AddSkillToSkillTitlesDtoRequest("React");
        AddSkillToSkillTitlesDtoRequest dtoRequest4 = new AddSkillToSkillTitlesDtoRequest("Node");
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest1));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest2));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest3));
        String response = server.addSkillToSkillsList(new Gson().toJson(dtoRequest4));

        assertEquals("{}", response);
    }

    @Test
    public void testFailAddingSkillToSkillList() {
        Server server = new Server();
        server.start();

        AddSkillToSkillTitlesDtoRequest dtoRequest1 = new AddSkillToSkillTitlesDtoRequest("Java");
        AddSkillToSkillTitlesDtoRequest dtoRequest2 = new AddSkillToSkillTitlesDtoRequest("Apache");
        AddSkillToSkillTitlesDtoRequest dtoRequest3 = new AddSkillToSkillTitlesDtoRequest("Node");
        AddSkillToSkillTitlesDtoRequest dtoRequest4 = new AddSkillToSkillTitlesDtoRequest("Node");
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest1));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest2));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest3));
        String response = server.addSkillToSkillsList(new Gson().toJson(dtoRequest4));

        assertTrue(response.contains(DataBaseErrorCode.SKILL_NAME_DOES_EXIST.toString()));
    }

    @Test
    public void testGettingSkillsList() {
        Server server = new Server();
        server.start();

        AddSkillToSkillTitlesDtoRequest dtoRequest1 = new AddSkillToSkillTitlesDtoRequest("Java");
        AddSkillToSkillTitlesDtoRequest dtoRequest2 = new AddSkillToSkillTitlesDtoRequest("Apache");
        AddSkillToSkillTitlesDtoRequest dtoRequest3 = new AddSkillToSkillTitlesDtoRequest("React");
        AddSkillToSkillTitlesDtoRequest dtoRequest4 = new AddSkillToSkillTitlesDtoRequest("Node");
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest1));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest2));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest3));
        server.addSkillToSkillsList(new Gson().toJson(dtoRequest4));

        String response = server.getSkillsList();

        assertTrue(response.contains("Java"));
        assertTrue(response.contains("Apache"));
        assertTrue(response.contains("React"));
        assertTrue(response.contains("Node"));

    }

    @Test
    public void testFailGettingSkillsList() {
        Server server = new Server();
        server.start();

        String response = server.getSkillsList();

        assertTrue(response.contains(DataBaseErrorCode.SKILLS_LIST_IS_EMPTY.toString()));
    }
}
