package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import net.some.kruglov.hiring.dto.request.*;
import net.some.kruglov.hiring.server.Server;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TestGettingVacanciesForEmployee {

    @Test
    public void testGettingVacanciesForEmployeeAllMatchingRequirements() throws PersonException {
        Server server = new Server();
        server.start();
        RegisterEmployerDtoRequest registerCompanyOneDto =
                new RegisterEmployerDtoRequest("Petr Petrovich", "petr1979@gmail.com", "impetr", "passWord2312%",
                                                "CompanyOne", "st. Himikov 56");
        String companyOneResponse = server.registerEmployer(new Gson().toJson(registerCompanyOneDto,
                RegisterEmployerDtoRequest.class));
        String token = companyOneResponse.substring(10, companyOneResponse.length() - 2);
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Middle Java Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 4, true));
        requirements.add(new Requirement("Node", 4, true));
        requirements.add(new Requirement("English", 4, true));
        vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Junior JS Developer", 70,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));

        RegisterEmployerDtoRequest registerCompanyTwoDto =
                new RegisterEmployerDtoRequest("Alexey Alexov", "alexwork@gmail.com", "imalex", "passWord2312%",
                        "CompanyTwo", "st. Lenina 5");
        String companyTwoResponse = server.registerEmployer(new Gson().toJson(registerCompanyTwoDto,
                RegisterEmployerDtoRequest.class));
        token = companyTwoResponse.substring(10, companyTwoResponse.length() - 2);
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 5, true));
        requirements.add(new Requirement("Node", 5, true));
        requirements.add(new Requirement("React", 5, true));
        requirements.add(new Requirement("HTML5", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior JS Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("Grails", 5, true));
        requirements.add(new Requirement("GWT", 5, true));
        requirements.add(new Requirement("English", 5, true));
        vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior Java Developer", 150,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));

        RegisterEmployeeDtoRequest superEmployee = new RegisterEmployeeDtoRequest("Vasya Puplin", "vasyapupkin@ya.ru",
                "vasyapupkin1990", "passWord12312%");
        String employeeResponse = server.registerEmployee(new Gson().toJson(superEmployee));
        token = employeeResponse.substring(10, employeeResponse.length() - 2);
        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("Spring", 5, null));
        skills.add(new RequirementDto("Apache", 5, null));
        skills.add(new RequirementDto("Grails", 5, null));
        skills.add(new RequirementDto("GWT", 5, null));
        skills.add(new RequirementDto("Node", 5, null));
        skills.add(new RequirementDto("English", 5, null));
        SkillsDtoRequest skillsDtoRequest = new SkillsDtoRequest(token, skills);
        server.addSkills(new Gson().toJson(skillsDtoRequest));

        GetVacanciesForEmployeeDtoRequest dtoRequest = new GetVacanciesForEmployeeDtoRequest(token);
        String vacanciesResponse = server.getVacanciesForEmployeeAllMatchingRequirements(new Gson().toJson(dtoRequest));


        assertTrue(vacanciesResponse.contains("Senior Java Developer"));
        assertTrue(vacanciesResponse.contains("Middle Java Developer"));
    }

    @Test
    public void testGettingVacanciesForEmployeeNecessaryRequirements() throws PersonException {
        Server server = new Server();
        server.start();
        RegisterEmployerDtoRequest registerCompanyOneDto =
                new RegisterEmployerDtoRequest("Petr Petrovich", "petr1979@gmail.com", "impetr", "passWord2312%",
                        "CompanyOne", "st. Himikov 56");
        String companyOneResponse = server.registerEmployer(new Gson().toJson(registerCompanyOneDto,
                RegisterEmployerDtoRequest.class));
        String token = companyOneResponse.substring(10, companyOneResponse.length() - 2);
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Middle Java Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 4, true));
        requirements.add(new Requirement("Node", 4, true));
        requirements.add(new Requirement("English", 4, true));
        vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Junior JS Developer", 70,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));

        RegisterEmployerDtoRequest registerCompanyTwoDto =
                new RegisterEmployerDtoRequest("Alexey Alexov", "alexwork@gmail.com", "imalex", "passWord2312%",
                        "CompanyTwo", "st. Lenina 5");
        String companyTwoResponse = server.registerEmployer(new Gson().toJson(registerCompanyTwoDto,
                RegisterEmployerDtoRequest.class));
        token = companyTwoResponse.substring(10, companyTwoResponse.length() - 2);
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 5, true));
        requirements.add(new Requirement("Node", 5, true));
        requirements.add(new Requirement("React", 5, true));
        requirements.add(new Requirement("HTML5", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior JS Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, false));
        requirements.add(new Requirement("Apache", 5, false));
        requirements.add(new Requirement("Grails", 5, false));
        requirements.add(new Requirement("GWT", 5, false));
        requirements.add(new Requirement("English", 5, true));
        vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior Java Developer", 150,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));

        RegisterEmployeeDtoRequest superEmployee = new RegisterEmployeeDtoRequest("Vasya Puplin", "vasyapupkin@ya.ru",
                "vasyapupkin1990", "passWord12312%");
        String employeeResponse = server.registerEmployee(new Gson().toJson(superEmployee));
        token = employeeResponse.substring(10, employeeResponse.length() - 2);
        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("English", 5, null));
        SkillsDtoRequest skillsDtoRequest = new SkillsDtoRequest(token, skills);
        server.addSkills(new Gson().toJson(skillsDtoRequest));

        GetVacanciesForEmployeeDtoRequest dtoRequest = new GetVacanciesForEmployeeDtoRequest(token);
        String vacanciesResponse = server.getVacanciesForEmployeeNecessaryRequirement(new Gson().toJson(dtoRequest));

        assertTrue(vacanciesResponse.contains("Senior Java Developer"));
    }

    @Test
    public void testGettingVacanciesForEmployeeAllMatchingRequirementsWithOutLevel() throws PersonException {
        Server server = new Server();
        server.start();
        RegisterEmployerDtoRequest registerCompanyOneDto =
                new RegisterEmployerDtoRequest("Petr Petrovich", "petr1979@gmail.com", "impetr", "passWord2312%",
                        "CompanyOne", "st. Himikov 56");
        String companyOneResponse = server.registerEmployer(new Gson().toJson(registerCompanyOneDto,
                RegisterEmployerDtoRequest.class));
        String token = companyOneResponse.substring(10, companyOneResponse.length() - 2);
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Middle Java Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 4, true));
        requirements.add(new Requirement("Node", 4, true));
        requirements.add(new Requirement("English", 4, true));
        vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Junior JS Developer", 70,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));

        RegisterEmployerDtoRequest registerCompanyTwoDto =
                new RegisterEmployerDtoRequest("Alexey Alexov", "alexwork@gmail.com", "imalex", "passWord2312%",
                        "CompanyTwo", "st. Lenina 5");
        String companyTwoResponse = server.registerEmployer(new Gson().toJson(registerCompanyTwoDto,
                RegisterEmployerDtoRequest.class));
        token = companyTwoResponse.substring(10, companyTwoResponse.length() - 2);
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 5, true));
        requirements.add(new Requirement("Node", 5, true));
        requirements.add(new Requirement("React", 5, true));
        requirements.add(new Requirement("HTML5", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior JS Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("Grails", 5, true));
        requirements.add(new Requirement("GWT", 5, true));
        requirements.add(new Requirement("English", 5, true));
        vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior Java Developer", 150,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));

        RegisterEmployeeDtoRequest neSuperEmployee = new RegisterEmployeeDtoRequest("Vasya Puplin", "vasyapupkin@ya.ru",
                "vasyapupkin1990", "passWord12312%");
        String employeeResponse = server.registerEmployee(new Gson().toJson(neSuperEmployee));
        token = employeeResponse.substring(10, employeeResponse.length() - 2);
        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 2, null));
        skills.add(new RequirementDto("Spring", 2, null));
        skills.add(new RequirementDto("Apache", 2, null));
        skills.add(new RequirementDto("Grails", 1, null));
        skills.add(new RequirementDto("GWT", 1, null));
        skills.add(new RequirementDto("Node", 1, null));
        skills.add(new RequirementDto("English", 2, null));
        SkillsDtoRequest skillsDtoRequest = new SkillsDtoRequest(token, skills);
        server.addSkills(new Gson().toJson(skillsDtoRequest));

        GetVacanciesForEmployeeDtoRequest dtoRequest = new GetVacanciesForEmployeeDtoRequest(token);
        String vacanciesResponse = server.getVacanciesForEmployeeAllMatchingRequirementsWithOutLevel(new Gson().toJson(dtoRequest));

        assertTrue(vacanciesResponse.contains("Senior Java Developer"));
        assertTrue(vacanciesResponse.contains("Middle Java Developer"));
    }

    @Test
    public void testGettingVacanciesForEmployeeAtLeastOneRequirement() throws PersonException {
        Server server = new Server();
        server.start();
        RegisterEmployerDtoRequest registerCompanyOneDto =
                new RegisterEmployerDtoRequest("Petr Petrovich", "petr1979@gmail.com", "impetr", "passWord2312%",
                        "CompanyOne", "st. Himikov 56");
        String companyOneResponse = server.registerEmployer(new Gson().toJson(registerCompanyOneDto,
                RegisterEmployerDtoRequest.class));
        String token = companyOneResponse.substring(10, companyOneResponse.length() - 2);
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Middle Java Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 4, true));
        requirements.add(new Requirement("Node", 4, true));
        requirements.add(new Requirement("English", 4, true));
        vacancyCompanyOneRequest = new VacancyDtoRequest(token, "Junior JS Developer", 70,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyOneRequest, VacancyDtoRequest.class));

        RegisterEmployerDtoRequest registerCompanyTwoDto =
                new RegisterEmployerDtoRequest("Alexey Alexov", "alexwork@gmail.com", "imalex", "passWord2312%",
                        "CompanyTwo", "st. Lenina 5");
        String companyTwoResponse = server.registerEmployer(new Gson().toJson(registerCompanyTwoDto,
                RegisterEmployerDtoRequest.class));
        token = companyTwoResponse.substring(10, companyTwoResponse.length() - 2);
        requirements.clear();
        requirements.add(new Requirement("JavaScript", 5, true));
        requirements.add(new Requirement("Angular", 5, true));
        requirements.add(new Requirement("Node", 5, true));
        requirements.add(new Requirement("React", 5, true));
        requirements.add(new Requirement("HTML5", 5, true));
        requirements.add(new Requirement("English", 4, true));
        VacancyDtoRequest vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior JS Developer", 110,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));
        requirements.clear();
        requirements.add(new Requirement("Java", 5, true));
        requirements.add(new Requirement("Spring", 5, true));
        requirements.add(new Requirement("Apache", 5, true));
        requirements.add(new Requirement("Grails", 5, true));
        requirements.add(new Requirement("GWT", 5, true));
        requirements.add(new Requirement("English", 5, true));
        vacancyCompanyTwoRequest = new VacancyDtoRequest(token, "Senior Java Developer", 150,
                requirements);
        server.addVacancy(new Gson().toJson(vacancyCompanyTwoRequest, VacancyDtoRequest.class));

        RegisterEmployeeDtoRequest superEmployee = new RegisterEmployeeDtoRequest("Vasya Puplin", "vasyapupkin@ya.ru",
                "vasyapupkin1990", "passWord12312%");
        String employeeResponse = server.registerEmployee(new Gson().toJson(superEmployee));
        token = employeeResponse.substring(10, employeeResponse.length() - 2);
        List<RequirementDto> skills = new ArrayList<>();
        skills.add(new RequirementDto("Java", 5, null));
        skills.add(new RequirementDto("Spring", 5, null));
        skills.add(new RequirementDto("Apache", 5, null));
        skills.add(new RequirementDto("Grails", 5, null));
        skills.add(new RequirementDto("GWT", 5, null));
        skills.add(new RequirementDto("Node", 5, null));
        skills.add(new RequirementDto("English", 5, null));
        SkillsDtoRequest skillsDtoRequest = new SkillsDtoRequest(token, skills);
        server.addSkills(new Gson().toJson(skillsDtoRequest));

        GetVacanciesForEmployeeDtoRequest dtoRequest = new GetVacanciesForEmployeeDtoRequest(token);
        String vacanciesResponse = server.getVacanciesForEmployeeAtLeastOneRequirement(new Gson().toJson(dtoRequest));

        assertTrue(vacanciesResponse.contains("Senior Java Developer"));
        assertTrue(vacanciesResponse.contains("Middle Java Developer"));
        assertTrue(vacanciesResponse.contains("Senior JS Developer"));
        assertTrue(vacanciesResponse.contains("Junior JS Developer"));
    }
}
