package net.some.kruglov.hiring.validator;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestValidator {

    @Test
    public void testValidName() {
        try {
            Validator.validateName("Alex");
            Validator.validateName("Alex Alex");
            Validator.validateName("Neo Neneo Oneo");
            Validator.validateName("Neo Neneo Oneo Baneo");
            assertTrue(true);
        } catch (ValidatorException exception) {
            fail();
        }
    }

    @Test
    public void testInvalidName() {
        try {
            Validator.validateName("");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_NAME, exception.getErrorCode());
        }
        try {
            Validator.validateName(null);
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_NAME, exception.getErrorCode());
        }
    }

    @Test
    public void testValidEmail() {
        try {
            Validator.validateEmail("a@mail.dd");
            Validator.validateEmail("a@mai2l.ddd");
            Validator.validateEmail("a@mai-l.dddd");
            Validator.validateEmail("a-dfg123@m32ai43l.ddddd");
            Validator.validateEmail("2312@mail.dddddd");
            assertTrue(true);
        } catch (ValidatorException exception) {
            fail();
        }
    }

    @Test
    public void testInvalidEmail() {
        try {
            Validator.validateEmail("");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_EMAIL, exception.getErrorCode());
        }
        try {
            Validator.validateEmail(null);
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_EMAIL, exception.getErrorCode());
        }
        try {
            Validator.validateEmail("jkdng.ru");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_EMAIL, exception.getErrorCode());
        }

        try {
            Validator.validateEmail("asd@asd.r");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_EMAIL, exception.getErrorCode());
        }
        try {
            Validator.validateEmail("@a.ru");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_EMAIL, exception.getErrorCode());
        }
        try {
            Validator.validateEmail("dsfs@aru");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_EMAIL, exception.getErrorCode());
        }
    }

    @Test
    public void testValidLogin() {
        try {
            Validator.validateLogin("imconstatnine");
            Validator.validateLogin("johnny1996");
            Validator.validateLogin("imcon");
            assertTrue(true);
        } catch (ValidatorException exception) {
            fail();
        }
    }

    @Test
    public void testInvalidLogin() {
        try {
            Validator.validateLogin("");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_LOGIN, exception.getErrorCode());
        }
        try {
            Validator.validateLogin(null);
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_LOGIN, exception.getErrorCode());
        }
        try {
            Validator.validateLogin("qwer");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_LOGIN, exception.getErrorCode());
        }
    }

    @Test
    public void testValidPassword() {
        try {
            Validator.validatePassword("passWord0000%");
            Validator.validatePassword("12345678aA^");
            Validator.validatePassword("qwertyY1%");
            Validator.validatePassword("@#$%^&+=Aa4");
            assertTrue(true);
        } catch (ValidatorException exception) {
            fail();
        }
    }

    @Test
    public void testInvalidPassword() {
        try {
            Validator.validatePassword("");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword(null);
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword("qwerty");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword("123456789");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword("KOaskdnadh^$%^");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword("^%$^%$^");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword("Pldsfnkjd67th7");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
        try {
            Validator.validatePassword("hdfbldHdfsj7hbds38n");
            fail();
        } catch (ValidatorException exception) {
            assertEquals(ValidatorErrorCode.NOT_VALIDATE_PASSWORD, exception.getErrorCode());
        }
    }
}
