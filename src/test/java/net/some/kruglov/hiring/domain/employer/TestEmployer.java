package net.some.kruglov.hiring.domain.employer;

import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.dto.request.RegisterEmployerDtoRequest;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class TestEmployer {

    @Test
    public void testEmployerConstructor() {
        Employer employer1 = new Employer("Full Name", "aaaa@aaa.bb", "login-324", "qwertY1234%",
                "Dunder mifflin inc.", "st. Lenina, 15");
        assertEquals("Full Name", employer1.getFullName());
        assertEquals("aaaa@aaa.bb", employer1.getEmail());
        assertEquals("login-324", employer1.getLogin());
        assertEquals(DigestUtils.md5Hex("qwertY1234%"), employer1.getHashKey());
        assertEquals("Dunder mifflin inc.", employer1.getCompanyName());
        assertEquals("st. Lenina, 15", employer1.getAddress());

        Employer employer2 = new Employer(new RegisterEmployerDtoRequest("Full Name", "aaaa@aaa.bb", "login-324",
                "qwertY1234%", "Dunder mifflin inc.", "st. Lenina, 15"));
        assertEquals("Full Name", employer2.getFullName());
        assertEquals("aaaa@aaa.bb", employer2.getEmail());
        assertEquals("login-324", employer2.getLogin());
        assertEquals(DigestUtils.md5Hex("qwertY1234%"), employer2.getHashKey());
        assertEquals("Dunder mifflin inc.", employer2.getCompanyName());
        assertEquals("st. Lenina, 15", employer2.getAddress());
    }

    @Test
    public void testEmployerSetters() throws PersonException {
        Employer employer = new Employer("Full Name", "aaaa@aaa.bb", "login-324", "qwertY1234%", "Dunder mifflin inc.",
                "st. Lenina, 15");
        String token = UUID.randomUUID().toString();
        employer.setToken(token);
        employer.setFullName("ANother Name");
        employer.setEmail("kkkk@gmail.com");
        employer.setCompanyName("Apple inc.");
        employer.setAddress("some address");
        employer.setHashKey(DigestUtils.md5Hex("newPassword19643^"));
        assertEquals("ANother Name", employer.getFullName());
        assertEquals("kkkk@gmail.com", employer.getEmail());
        assertEquals(DigestUtils.md5Hex("newPassword19643^"), employer.getHashKey());
        assertEquals("Apple inc.", employer.getCompanyName());
        assertEquals("some address", employer.getAddress());
        assertEquals(token, employer.getToken());
    }

    @Test
    public void testEmployerAddVacancy() throws PersonException {
        Employer employer = new Employer("employerName", "employerEmail", "emplyerLogin","employerPassword",
                "employerCompanyName", "employerAddress");
        employer.addVacancy(new Vacancy("Junior Java Developer", 100));
        assertTrue(employer.getVacancyList().contains(new Vacancy("Junior Java Developer", 100)));

        try {
            employer.addVacancy(new Vacancy("Junior Java Developer", 100));
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.DUPLICATE_VACANCY, exception.getErrorCode());
        }
    }

    @Test
    public void testEmployerGetVacancy() throws PersonException {
        Employer employer = new Employer("employerName", "employerEmail", "emplyerLogin","employerPassword",
                "employerCompanyName", "employerAddress");
        Vacancy vacancy1 = new Vacancy("Junior Java Developer", 100);
        employer.addVacancy(vacancy1);
        Vacancy vacancy2 = employer.getVacancy("Junior Java Developer");
        assertEquals(vacancy1, vacancy2);

        try {
            employer.getVacancy("Ruby Developer");
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.VACANCY_NOT_FOUND, exception.getErrorCode());
        }
    }

    @Test
    public void testEmployerRemoveVacancy() throws PersonException {
        Employer employer = new Employer("employerName", "employerEmail", "emplyerLogin","employerPassword",
                "employerCompanyName", "employerAddress");
        employer.addVacancy(new Vacancy("Junior Java Developer", 100));
        employer.removeVacancy("Junior Java Developer");
        assertFalse(employer.getVacancyList().contains(new Vacancy("Junior Java Developer", 100)));

        try {
            employer.removeVacancy("Junior Java Developer");
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.VACANCY_NOT_FOUND, exception.getErrorCode());
        }
    }

}
