package net.some.kruglov.hiring.domain.vacancy;

import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestVacancy {
    @Test
    public void testVacancyConstructor() throws PersonException {
        Vacancy vacancy = new Vacancy("Junior Java Developer", 100);
        assertEquals("Junior Java Developer", vacancy.getVacancyName());
        assertEquals(100, vacancy.getSalary());

        try {
            Vacancy vacancy1 = new Vacancy("", 1);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.VACANCY_WRONG_NAME, exception.getErrorCode());
        }
        try {
            Vacancy vacancy1 = new Vacancy("Some Name", -1);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.NEGATIVE_SALARY, exception.getErrorCode());
        }
    }

    @Test
    public void testVacancySetters() throws PersonException {
        Vacancy vacancy = new Vacancy("Junior Java Developer", 100);
        vacancy.setActivationState(true);
        vacancy.setVacancyName("Ruby back-end");
        vacancy.setSalary(101);
        assertEquals("Ruby back-end", vacancy.getVacancyName());
        assertEquals(101, vacancy.getSalary());
        assertTrue(vacancy.getActivationState());
    }

    @Test
    public void testVacancyAddRequirement() throws PersonException {
        Vacancy vacancy = new Vacancy("Junior Java Developer", 100);
        Requirement requirementEnglish = new Requirement("English Language", 5, true);
        vacancy.addRequirement(requirementEnglish);
        assertTrue(vacancy.getRequirementList().contains(requirementEnglish));

        try {
            vacancy.addRequirement(requirementEnglish);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.REQUIREMENT_DUPLICATE, exception.getErrorCode());
        }
    }

    @Test
    public void testVacancyRemoveRequirement() throws PersonException {
        Vacancy vacancy = new Vacancy("Junior Java Developer", 100);
        Requirement requirementEnglish = new Requirement("English Language", 5, true);
        vacancy.addRequirement(requirementEnglish);
        vacancy.removeRequirement("English Language");
        assertFalse(vacancy.getRequirementList().contains(requirementEnglish));

        try {
            vacancy.removeRequirement("English Language");
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.REQUIREMENT_NOT_FOUND, exception.getErrorCode());
        }
    }

    @Test
    public void testVacancyGetRequirement() throws PersonException {
        Vacancy vacancy = new Vacancy("Junior Java Developer", 100);
        Requirement requirementEnglish = new Requirement("English Language", 5, true);
        vacancy.addRequirement(requirementEnglish);
        assertEquals(requirementEnglish, vacancy.getRequirement("English Language"));

        try {
            vacancy.getRequirement("Spring");
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.REQUIREMENT_NOT_FOUND, exception.getErrorCode());
        }
    }
}
