package net.some.kruglov.hiring.domain.employee;

import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.dto.request.RegisterEmployeeDtoRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class TestEmployee {

    @Test
    public void testEmployeeConstructor() {
        Employee employee1 = new Employee("Full Name", "email@aaa.ru", "imconstantine", "passWord1996%");
        assertEquals("Full Name", employee1.getFullName());
        assertEquals("email@aaa.ru", employee1.getEmail());
        assertEquals("imconstantine", employee1.getLogin());
        assertEquals(DigestUtils.md5Hex("passWord1996%"), employee1.getHashKey());

        Employee employee2 = new Employee(new RegisterEmployeeDtoRequest("Full Name", "email@aaa.ru",
                "imconstantine", "passWord1996%"));
        assertEquals("Full Name", employee2.getFullName());
        assertEquals("email@aaa.ru", employee2.getEmail());
        assertEquals("imconstantine", employee2.getLogin());
        assertEquals(DigestUtils.md5Hex("passWord1996%"), employee2.getHashKey());
    }

    @Test
    public void testEmployeeSetters() {
        Employee employee = new Employee("Full Name", "email@aaa.ru", "imconstantine", "passWord1996%");
        String token = UUID.randomUUID().toString();
        employee.setToken(token);
        employee.setEmail("aaaaa@gmail.com");
        employee.setFullName("Another Full Name");
        employee.setHashKey(DigestUtils.md5Hex("anotherPassword1995$"));
        assertEquals(token, employee.getToken());
        assertEquals("aaaaa@gmail.com", employee.getEmail());
        assertEquals("Another Full Name", employee.getFullName());
        assertEquals(DigestUtils.md5Hex("anotherPassword1995$"), employee.getHashKey());
    }

    @Test
    public void testEmployeeAddSkill() throws PersonException {
        Employee employee = new Employee("Full Name", "email@aaa.ru", "imconstantine", "passWord1996%");
        employee.addSkill("Java", 5);
        assertTrue(employee.getSkills().containsKey("Java"));
        assertEquals((int) employee.getSkills().get("Java"), 5);
        assertFalse(employee.getSkills().containsKey("Ruby"));

        try {
            employee.addSkill("Java", 3);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.DUPLICATE_SKILL, exception.getErrorCode());
        }
    }

    @Test
    public void testEmployeeRemoveSkill() throws PersonException {
        Employee employee = new Employee("Full Name", "email@aaa.ru", "imconstantine", "passWord1996%");
        employee.addSkill("Java", 5);
        employee.removeSkill("Java");
        assertFalse(employee.getSkills().containsKey("Java"));
        assertEquals(0, employee.getSkills().size());

        try {
            employee.removeSkill("Ruby");
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.SKILL_NOT_FOUND, exception.getErrorCode());
        }
    }

    @Test
    public void testEmployeeChangeLevel() throws PersonException {
        Employee employee = new Employee("Full Name", "email@aaa.ru", "imconstantine", "passWord1996%");
        employee.addSkill("Ruby", 5);
        employee.changeLevel("Ruby", 4);
        assertEquals(4, (int) employee.getSkills().get("Ruby"));

        try {
            employee.changeLevel("Java", 4);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.SKILL_NOT_FOUND, exception.getErrorCode());
        }
        try {
            employee.changeLevel("Ruby", 6);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.SKILL_WRONG_LEVEL, exception.getErrorCode());
        }

        try {
            employee.changeLevel("Ruby", 0);
            fail();
        } catch (PersonException exception) {
            assertEquals(PersonErrorCode.SKILL_WRONG_LEVEL, exception.getErrorCode());
        }
    }
}
