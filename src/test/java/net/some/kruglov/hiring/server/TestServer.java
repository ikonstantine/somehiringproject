package net.some.kruglov.hiring.server;

import net.some.kruglov.hiring.domain.person.PersonException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestServer {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testSaveServerToFile() throws PersonException, IOException {
        Server server = new Server();
        server.start();

        String registerEmployerRequest = "{\"fullName\":\"Petr Petrovich\",\"email\":\"petr1979@gmail.com\"," +
                "\"login\":\"impetr\",\"password\":\"passWord2312%\",\"companyName\":\"CompanyOne\"," +
                "\"address\":\"st. Himikov 56\"}";
        String companyOneResponse = server.registerEmployer(registerEmployerRequest);
        String token = companyOneResponse.substring(10, companyOneResponse.length() - 2);
        String addVacancyRequest = "{\"token\":\"" +token+ "\",\"nameVacancy\":\"Middle Java Developer\"," +
                "\"salary\":110,\"requirementList\":[{\"skillName\":\"Java\",\"level\":5,\"require\":true}," +
                "{\"skillName\":\"Spring\",\"level\":5,\"require\":true},{\"skillName\":\"Apache\",\"level\":5," +
                "\"require\":true},{\"skillName\":\"English\",\"level\":4,\"require\":true}]}";
        server.addVacancy(addVacancyRequest);
        addVacancyRequest = "{\"token\":\"" +token+ "\",\"nameVacancy\":\"Junior JS Developer\"," +
                "\"salary\":70,\"requirementList\":[{\"skillName\":\"JavaScript\",\"level\":5,\"require\":true}," +
                "{\"skillName\":\"Angular\",\"level\":4,\"require\":true},{\"skillName\":\"Node\",\"level\":4," +
                "\"require\":true},{\"skillName\":\"English\",\"level\":4,\"require\":true}]}";
        server.addVacancy(addVacancyRequest);
        String registerEmployeeRequest = "{\"fullName\":\"Vasya Puplin\",\"login\":\"vasyapupkin1990\"," +
                "\"password\":\"passWord12312%\",\"email\":\"vasyapupkin@ya.ru\"}";
        String employeeResponse = server.registerEmployee(registerEmployeeRequest);
        token = employeeResponse.substring(10, employeeResponse.length() - 2);
        String addSkillsEmployeeRequest = "{\"token\":\"" +token+ "\",\"skills\":[{\"skillName\":\"Java\",\"level\":5}," +
                "{\"skillName\":\"Spring\",\"level\":5},{\"skillName\":\"Apache\",\"level\":5},{\"skillName\":\"Grails\"," +
                "\"level\":5},{\"skillName\":\"GWT\",\"level\":5},{\"skillName\":\"Node\",\"level\":5}," +
                "{\"skillName\":\"English\",\"level\":5}]}";
        server.addSkills(addSkillsEmployeeRequest);

        File saveFile = folder.newFile("save1.txt");
        server.stop(saveFile.getAbsolutePath());

        assertTrue(saveFile.exists());
        assertFalse(server.isRunning());
    }

    @Test
    public void testLoadServerFromFile() throws PersonException, IOException {
        Server server = new Server();
        server.start();
        String registerEmployerRequest = "{\"fullName\":\"Petr Petrovich\",\"email\":\"petr1979@gmail.com\"," +
                "\"login\":\"impetr\",\"password\":\"passWord2312%\",\"companyName\":\"CompanyOne\"," +
                "\"address\":\"st. Himikov 56\"}";
        server.registerEmployer(registerEmployerRequest);
        String registerEmployeeRequest = "{\"fullName\":\"Vasya Puplin\",\"login\":\"vasyapupkin1990\"," +
                "\"password\":\"passWord12312%\",\"email\":\"vasyapupkin@ya.ru\"}";
        String employeeRegResponse = server.registerEmployee(registerEmployeeRequest);
        String token = employeeRegResponse.substring(10, employeeRegResponse.length() - 2);
        File saveFile = folder.newFile("save1.txt");
        server.stop(saveFile.getAbsolutePath());



        Server anotherServer = new Server();
        anotherServer.start(saveFile.getAbsolutePath());
        assertTrue(anotherServer.isRunning());

        String addSkillsEmployeeRequest = "{\"token\":\"" +token+ "\",\"skills\":[{\"skillName\":\"Java\",\"level\":5}," +
                "{\"skillName\":\"Spring\",\"level\":5},{\"skillName\":\"Apache\",\"level\":5},{\"skillName\":\"Grails\"," +
                "\"level\":5},{\"skillName\":\"GWT\",\"level\":5},{\"skillName\":\"Node\",\"level\":5}," +
                "{\"skillName\":\"English\",\"level\":5}]}";
        String addSkillResponse = anotherServer.addSkills(addSkillsEmployeeRequest);
        assertEquals(addSkillResponse, "{}");
    }
}
