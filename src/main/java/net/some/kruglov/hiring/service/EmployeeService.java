package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;
import net.some.kruglov.hiring.daoimpl.EmployeeDaoImpl;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;
import net.some.kruglov.hiring.dto.request.*;
import net.some.kruglov.hiring.dto.response.*;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;
import java.util.Set;

public class EmployeeService {
    private DataBase dataBase;
    private EmployeeDaoImpl employeeDaoImpl;

    public EmployeeService(DataBase dataBase) {
        this.dataBase = dataBase;
        employeeDaoImpl = new EmployeeDaoImpl(dataBase);
    }

    public String registerEmployee(String jsonRequest) {
        RegisterEmployeeDtoRequest employeeDtoRequest = new Gson().
                fromJson(jsonRequest, RegisterEmployeeDtoRequest.class);
        try {
            employeeDtoRequest.validate();
            Employee employee = employeeDaoImpl.insert(new Employee(employeeDtoRequest));
            employee.setActivationState(true);
            return TokenDtoResponse.jsonResponse(employee.getToken());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String leaveEmployee(String jsonRequest) {
        ExitPersonDtoRequest exitPersonRequest = new Gson().fromJson(jsonRequest, ExitPersonDtoRequest.class);
        try {
            Validator.validateToken(exitPersonRequest.getToken());
            employeeDaoImpl.delete(exitPersonRequest.getToken());
            return EmptyDtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String addSkills(String jsonRequest) {
        SkillsDtoRequest skillsDtoRequest = new Gson().fromJson(jsonRequest, SkillsDtoRequest.class);
        try {
            Validator.validateToken(skillsDtoRequest.getToken());
            Employee employee = employeeDaoImpl.getEmployee(skillsDtoRequest.getToken());
            for (RequirementDto requirementDto : skillsDtoRequest.getSkills())
                employee.addSkill(requirementDto.getSkillName(), requirementDto.getLevel());
            return EmptyDtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (PersonException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String editEmployeeData(String jsonRequest) {
        ChangeEmployeeDataDtoRequest cedtr = new Gson().fromJson(jsonRequest, ChangeEmployeeDataDtoRequest.class);
        try {
            cedtr.validate();
            Employee employee = employeeDaoImpl.getEmployee(cedtr.getToken());
            if (cedtr.getFullName() != null) employee.setFullName(cedtr.getFullName());
            if (cedtr.getEmail() != null) employee.setEmail(cedtr.getEmail());
            if (cedtr.getPassword() != null) employee.setHashKey(DigestUtils.md5Hex(cedtr.getPassword()));
            if (cedtr.getActivate() != null) employee.setActivationState(cedtr.getActivate());

            Set<String> skillsNames = cedtr.getSkills().keySet();
            RequirementDto requirementDto;
            for (String skillName : skillsNames) {
                requirementDto = cedtr.getSkills().get(skillName);
                if (skillName.equals("ADD_SKILL")) {
                    employee.addSkill(requirementDto.getSkillName(), requirementDto.getLevel());
                    continue;
                }

                if (requirementDto.getLevel() != null) employee.changeLevel(skillName, requirementDto.getLevel());
                if (requirementDto.getSkillName() != null) {
                    if (requirementDto.getSkillName().equals("DEL_SKILL")) employee.removeSkill(skillName);
                    else employee.changeSkillName(skillName, requirementDto.getSkillName());
                }
            }
            return EmptyDtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (PersonException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getVacanciesForEmployeeAllMatchingRequirements(String jsonRequest) {
        GetVacanciesForEmployeeDtoRequest dtoRequest = new Gson().
                fromJson(jsonRequest, GetVacanciesForEmployeeDtoRequest.class);
        VacanciesListDtoResponse dtoResponse = new VacanciesListDtoResponse();
        try {
            dtoRequest.validate();
            Employee employee = employeeDaoImpl.getEmployee(dtoRequest.getToken());
            List<Employer> employers = employeeDaoImpl.getEmployers();

            Integer level;
            int counterMatch;
            for (Employer employer : employers) {
                for (Vacancy vacancy : employer.getVacancyList()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null) {
                            if (level < requirement.getLevel()) break;
                            else counterMatch++;
                        }
                    }
                    if (counterMatch == vacancy.getRequirementList().size()) dtoResponse.
                            putVacancy(new MatchingVacancyDto(vacancy.getVacancyName(),
                            employer.getEmail(), employer.getCompanyName()));
                }
            }
            return dtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getVacanciesForEmployeeNecessaryRequirement(String jsonRequest) {
        GetVacanciesForEmployeeDtoRequest dtoRequest = new Gson().
                fromJson(jsonRequest, GetVacanciesForEmployeeDtoRequest.class);
        VacanciesListDtoResponse dtoResponse = new VacanciesListDtoResponse();
        try {
            dtoRequest.validate();
            Employee employee = employeeDaoImpl.getEmployee(dtoRequest.getToken());
            List<Employer> employers = employeeDaoImpl.getEmployers();

            Integer level;
            int counterMatch;
            for (Employer employer : employers) {
                for (Vacancy vacancy : employer.getVacancyList()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null && requirement.getRequireState()) {
                            if (level < requirement.getLevel()) break;
                            else counterMatch++;
                        }
                    }
                    if (counterMatch == vacancy.getRequirementList().stream().filter(Requirement::getRequireState).count())
                        dtoResponse.putVacancy(new MatchingVacancyDto(vacancy.getVacancyName(),
                                employer.getEmail(), employer.getCompanyName()));
                }
            }
            return dtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getVacanciesForEmployeeAllMatchingRequirementsWithOutLevel(String jsonRequest) {
        GetVacanciesForEmployeeDtoRequest dtoRequest = new Gson().
                fromJson(jsonRequest, GetVacanciesForEmployeeDtoRequest.class);
        VacanciesListDtoResponse dtoResponse = new VacanciesListDtoResponse();
        try {
            dtoRequest.validate();
            Employee employee = employeeDaoImpl.getEmployee(dtoRequest.getToken());
            List<Employer> employers = employeeDaoImpl.getEmployers();

            Integer level;
            int counterMatch;
            for (Employer employer : employers) {
                for (Vacancy vacancy : employer.getVacancyList()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null) counterMatch++;
                    }
                    if (counterMatch == vacancy.getRequirementList().size()) dtoResponse.
                            putVacancy(new MatchingVacancyDto(vacancy.getVacancyName(),
                            employer.getEmail(), employer.getCompanyName()));
                }
            }
            return dtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getVacanciesForEmployeeAtLeastOneRequirement(String jsonRequest) {
        GetVacanciesForEmployeeDtoRequest dtoRequest = new Gson().
                fromJson(jsonRequest, GetVacanciesForEmployeeDtoRequest.class);
        VacanciesListDtoResponse dtoResponse = new VacanciesListDtoResponse();
        try {
            dtoRequest.validate();
            Employee employee = employeeDaoImpl.getEmployee(dtoRequest.getToken());
            List<Employer> employers = employeeDaoImpl.getEmployers();

            Integer level;
            int counterMatch;
            for (Employer employer : employers) {
                for (Vacancy vacancy : employer.getVacancyList()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null) {
                            if (level < requirement.getLevel()) break;
                            else counterMatch++;
                        }
                    }
                    if (counterMatch > 0) dtoResponse.putVacancy(new MatchingVacancyDto(vacancy.getVacancyName(),
                                    employer.getEmail(), employer.getCompanyName(), counterMatch));
                }
            }
            dtoResponse.getList().sort((matchingVacancyDto, t1) -> t1.getMatchingCount().compareTo(matchingVacancyDto.getMatchingCount()));

            return dtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }
}
