package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.daoimpl.PersonExitDaoImpl;
import net.some.kruglov.hiring.dto.request.ExitPersonDtoRequest;
import net.some.kruglov.hiring.dto.response.EmptyDtoResponse;
import net.some.kruglov.hiring.dto.response.ErrorDtoResponse;

public class PersonOutService {
    private DataBase dataBase;

    public PersonOutService(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public String exitFromServer(String jsonRequest) {
        ExitPersonDtoRequest personTokenExit = new Gson().fromJson(jsonRequest, ExitPersonDtoRequest.class);
        try {
            new PersonExitDaoImpl(dataBase).exit(personTokenExit.getToken());
            return EmptyDtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }
}
