package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;
import net.some.kruglov.hiring.daoimpl.AuthorizationDaoImpl;
import net.some.kruglov.hiring.domain.person.Person;
import net.some.kruglov.hiring.dto.request.AuthorizationPersonDtoRequest;
import net.some.kruglov.hiring.dto.response.ErrorDtoResponse;
import net.some.kruglov.hiring.dto.response.TokenDtoResponse;
import org.apache.commons.codec.digest.DigestUtils;

public class AuthorizationService {
    DataBase dataBase = new DataBase();

    public AuthorizationService(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public String authorizationPerson(String jsonRequest) {
        AuthorizationPersonDtoRequest authorizationPersonDtoRequest = new Gson().
                fromJson(jsonRequest, AuthorizationPersonDtoRequest.class);
        try {
            Validator.validateLogin(authorizationPersonDtoRequest.getLogin());
            Person person = new AuthorizationDaoImpl(dataBase).auth(authorizationPersonDtoRequest);

            Validator.equalsHashKeys(person.getHashKey(),
                    DigestUtils.md5Hex(authorizationPersonDtoRequest.getPassword()));
            return TokenDtoResponse.jsonResponse(person.getToken());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

}
