package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;
import net.some.kruglov.hiring.daoimpl.EmployerDaoImpl;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;
import net.some.kruglov.hiring.dto.request.*;
import net.some.kruglov.hiring.dto.response.*;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.*;

public class EmployerService {
    private DataBase dataBase;
    private EmployerDaoImpl employerDaoImpl;

    public EmployerService(DataBase dataBase) {
        this.dataBase = dataBase;
        this.employerDaoImpl = new EmployerDaoImpl(dataBase);
    }

    public String registerEmployer(String jsonRequest) {
        RegisterEmployerDtoRequest employerDtoRequest = new Gson().
                fromJson(jsonRequest, RegisterEmployerDtoRequest.class);
        try {
            employerDtoRequest.validate();
            Employer employer = employerDaoImpl.insert(new Employer(employerDtoRequest));
            return TokenDtoResponse.jsonResponse(employer.getToken());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String leaveEmployer(String jsonRequest) {
        ExitPersonDtoRequest exitPersonRequest = new Gson().fromJson(jsonRequest, ExitPersonDtoRequest.class);
        try {
            Validator.validateToken(exitPersonRequest.getToken());
            employerDaoImpl.delete(exitPersonRequest.getToken());
            return EmptyDtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String addVacancy(String jsonRequest) {
        VacancyDtoRequest vacancyRequest = new Gson().fromJson(jsonRequest, VacancyDtoRequest.class);
        try {
            vacancyRequest.validate();
            employerDaoImpl.addVacancy(new Vacancy(vacancyRequest), vacancyRequest.getToken());
            return EmptyDtoResponse.jsonResponse();
        } catch (PersonException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String editEmployerData(String jsonRequest) {
        ChangeEmployerDataDtoRequest employerDataDtoRequest = new Gson().fromJson(jsonRequest, ChangeEmployerDataDtoRequest.class);
        try {
            employerDataDtoRequest.validate();
            Employer employer = employerDaoImpl.getEmployer(employerDataDtoRequest.getToken());
            if (employerDataDtoRequest.getAddress() != null)
                employer.setAddress(employerDataDtoRequest.getAddress());
            if (employerDataDtoRequest.getCompanyName() != null)
                employer.setCompanyName(employerDataDtoRequest.getCompanyName());
            if (employerDataDtoRequest.getEmail() != null)
                employer.setEmail(employerDataDtoRequest.getEmail());
            if (employerDataDtoRequest.getFullName() != null)
                employer.setFullName(employerDataDtoRequest.getFullName());
            if (employerDataDtoRequest.getPassword() != null)
                employer.setHashKey(DigestUtils.md5Hex(employerDataDtoRequest.getPassword()));
            return EmptyDtoResponse.jsonResponse();
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (PersonException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String editVacancyData(String jsonRequest) {
        ChangeVacancyDtoRequest changeVacancyDtoRequest = new Gson().fromJson(jsonRequest, ChangeVacancyDtoRequest.class);
        try {
            Employer employer = employerDaoImpl.getEmployer(changeVacancyDtoRequest.getToken());
            Vacancy vacancy = employerDaoImpl.getVacancy(changeVacancyDtoRequest.getToken(),
                    changeVacancyDtoRequest.getCurrentVacancyName());
            Map<String, RequirementDto> requirements = changeVacancyDtoRequest.getRequirementMap();

            if (changeVacancyDtoRequest.getNewVacancyName() != null) {
                if (changeVacancyDtoRequest.getNewVacancyName().equals("DEL_VACANCY")) {
                    employer.removeVacancy(changeVacancyDtoRequest.getCurrentVacancyName());
                    return EmptyDtoResponse.jsonResponse();
                } else vacancy.setVacancyName(changeVacancyDtoRequest.getNewVacancyName());
            }
            if (changeVacancyDtoRequest.getNewSalary() != null)
                vacancy.setSalary(changeVacancyDtoRequest.getNewSalary());
            if (changeVacancyDtoRequest.getActivantion() != null)
                vacancy.setActivationState(changeVacancyDtoRequest.getActivantion());

            Set<String> skillNames = requirements.keySet();
            RequirementDto requirementDto;
            Requirement requirement;
            for (String skillName : skillNames) {
                requirementDto = requirements.get(skillName);

                if (skillName.equals("ADD_REQ")) {
                    vacancy.addRequirement(new Requirement(requirementDto.getSkillName(), requirementDto.getLevel(),
                            requirementDto.getRequireState()));
                    continue;
                }

                requirement = vacancy.getRequirement(skillName);
                if (requirementDto.getSkillName() != null) {
                    if (requirementDto.getSkillName().equals("DEL_REQ")) {
                        vacancy.removeRequirement(skillName);
                        continue;
                    } else requirement.setSkillName(requirementDto.getSkillName());
                }
                if (requirementDto.getLevel() != null) requirement.setLevel(requirementDto.getLevel());
                if (requirementDto.getRequireState() != null) requirement.setRequire(requirementDto.getRequireState());
            }
            return EmptyDtoResponse.jsonResponse();
        } catch (PersonException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getEmployeesForEachVacancy(String jsonRequest) {
        GetEmployeesForEachVacancyDtoRequest dtoRequest =
                new Gson().fromJson(jsonRequest, GetEmployeesForEachVacancyDtoRequest.class);
        EmployeesListDtoResponse dtoResponse = new EmployeesListDtoResponse();
        try {
            dtoRequest.validate();
            Integer level;
            int counterMatch;
            List<PersonDto> employeesList;
            for (Vacancy vacancy : employerDaoImpl.getEmployer(dtoRequest.getToken()).getVacancyList()) {
                employeesList = new ArrayList<>();
                for (Employee employee : employerDaoImpl.getEmployees()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null) {
                            if (level < requirement.getLevel()) break;
                            else counterMatch++;
                        }
                    }
                    if (counterMatch == vacancy.getRequirementList().size()) employeesList.
                            add(new PersonDto(employee.getFullName(), employee.getEmail(), employee.getToken()));
                }
                if (!employeesList.isEmpty()) dtoResponse.putVacancy(vacancy.getVacancyName(), employeesList);
                else dtoResponse.putVacancy(vacancy.getVacancyName(), null);
            }

            return dtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getEmployeesForEachVacancyForNecessaryRequirement(String jsonRequest) {
        GetEmployeesForEachVacancyDtoRequest dtoRequest =
                new Gson().fromJson(jsonRequest, GetEmployeesForEachVacancyDtoRequest.class);
        EmployeesListDtoResponse dtoResponse = new EmployeesListDtoResponse();
        try {
            dtoRequest.validate();
            Integer level;
            int counterMatch;
            List<PersonDto> employeesList;
            for (Vacancy vacancy : employerDaoImpl.getEmployer(dtoRequest.getToken()).getVacancyList()) {
                employeesList = new ArrayList<>();
                for (Employee employee : employerDaoImpl.getEmployees()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null && requirement.getRequireState()) {
                            if (level < requirement.getLevel()) break;
                            else counterMatch++;
                        }
                    }
                    if (counterMatch == vacancy.getRequirementList().stream().filter(Requirement::getRequireState).count())
                        employeesList.add(new PersonDto(employee.getFullName(), employee.getEmail(), employee.getToken()));
                }
                if (!employeesList.isEmpty()) dtoResponse.putVacancy(vacancy.getVacancyName(), employeesList);
                else dtoResponse.putVacancy(vacancy.getVacancyName(), null);
            }

            return dtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getEmployeesForEachVacancyForNecessaryRequirementWithOutLevel(String jsonRequest) {
        GetEmployeesForEachVacancyDtoRequest dtoRequest =
                new Gson().fromJson(jsonRequest, GetEmployeesForEachVacancyDtoRequest.class);
        EmployeesListDtoResponse dtoResponse = new EmployeesListDtoResponse();
        try {
            dtoRequest.validate();
            Integer level;
            int counterMatch;
            List<PersonDto> employeesList;
            for (Vacancy vacancy : employerDaoImpl.getEmployer(dtoRequest.getToken()).getVacancyList()) {
                employeesList = new ArrayList<>();
                for (Employee employee : employerDaoImpl.getEmployees()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null) counterMatch++;
                    }
                    if (counterMatch == vacancy.getRequirementList().size())
                        employeesList.add(new PersonDto(employee.getFullName(), employee.getEmail(), employee.getToken()));
                }
                if (!employeesList.isEmpty()) dtoResponse.putVacancy(vacancy.getVacancyName(), employeesList);
                else dtoResponse.putVacancy(vacancy.getVacancyName(), null);
            }
            return dtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String getEmployeesForEachVacancyAtLeastOneRequirement(String jsonRequest) {
        GetEmployeesForEachVacancyDtoRequest dtoRequest =
                new Gson().fromJson(jsonRequest, GetEmployeesForEachVacancyDtoRequest.class);
        EmployeesListDtoResponse dtoResponse = new EmployeesListDtoResponse();
        try {
            dtoRequest.validate();
            Integer level;
            int counterMatch;
            List<PersonDto> employeesList;
            for (Vacancy vacancy : employerDaoImpl.getEmployer(dtoRequest.getToken()).getVacancyList()) {
                employeesList = new ArrayList<>();
                for (Employee employee : employerDaoImpl.getEmployees()) {
                    counterMatch = 0;
                    for (Requirement requirement : vacancy.getRequirementList()) {
                        level = employee.getSkills().get(requirement.getSkillName());
                        if (level != null) {
                            if (level < requirement.getLevel()) break;
                            else counterMatch++;
                        }
                    }
                    if (counterMatch > 0) employeesList.add(new PersonDto(employee.getFullName(), employee.getEmail(),
                            employee.getToken()));
                }
                if (!employeesList.isEmpty()) dtoResponse.putVacancy(vacancy.getVacancyName(), employeesList);
                else dtoResponse.putVacancy(vacancy.getVacancyName(), null);
            }

            return dtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (ValidatorException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String acceptEmployee(String jsonRequest) {
        AcceptEmployeeDtoRequest dtoRequest = new Gson().fromJson(jsonRequest, AcceptEmployeeDtoRequest.class);
        try {
            employerDaoImpl.getVacancy(dtoRequest.getEmployerToken(), dtoRequest.getVacancyName()).setActivationState(false);
            employerDaoImpl.getEmployee(dtoRequest.getEmployeeToken()).setActivationState(false);
            return EmptyDtoResponse.jsonResponse();
        } catch (PersonException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }
}
