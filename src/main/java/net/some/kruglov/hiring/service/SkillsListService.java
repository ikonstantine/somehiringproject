package net.some.kruglov.hiring.service;

import com.google.gson.Gson;
import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.dto.request.AddSkillToSkillTitlesDtoRequest;
import net.some.kruglov.hiring.dto.response.EmptyDtoResponse;
import net.some.kruglov.hiring.dto.response.ErrorDtoResponse;
import net.some.kruglov.hiring.dto.response.SkillTitlesListDtoResponse;

public class SkillsListService {
    private DataBase dataBase;

    public SkillsListService(DataBase dataBase){
        this.dataBase = dataBase;
    }

    public String getSkillTitles() {
        try {
            SkillTitlesListDtoResponse dtoResponse = new SkillTitlesListDtoResponse(dataBase.getSkillNamesList());
            return dtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }

    public String addSkill(String jsonRequest) {
        AddSkillToSkillTitlesDtoRequest dtoRequest = new Gson().fromJson(jsonRequest, AddSkillToSkillTitlesDtoRequest.class);
        try {
            dataBase.addSkillName(dtoRequest.getSkill());
            return EmptyDtoResponse.jsonResponse();
        } catch (DataBaseException exception) {
            return ErrorDtoResponse.jsonResponse(exception.getErrorCode().toString());
        }
    }
}
