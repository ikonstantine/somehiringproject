package net.some.kruglov.hiring.domain.person;

public class PersonException extends Exception {
    private PersonErrorCode code;

    public PersonException(PersonErrorCode code) {
        this.code = code;
    }

    public PersonErrorCode getErrorCode() {
        return code;
    }
}
