package net.some.kruglov.hiring.domain.vacancy;

import net.some.kruglov.hiring.activation.Activatable;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;
import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.dto.request.VacancyDtoRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Vacancy implements Activatable, Serializable {
    private String vacancyName;
    private int salary;
    private boolean activation;
    private List<Requirement> requirementList;

    public Vacancy(String vacancyName, int salary) throws PersonException {
        setVacancyName(vacancyName);
        setSalary(salary);
        requirementList = new ArrayList<>();
    }

    public Vacancy(VacancyDtoRequest vacancyDtoRequest) throws PersonException {
        setVacancyName(vacancyDtoRequest.getNameVacancy());
        setSalary(vacancyDtoRequest.getSalary());
        this.requirementList = vacancyDtoRequest.getRequirementList();
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public void setVacancyName(String vacancyName) throws PersonException {
        if (vacancyName == null) throw new PersonException(PersonErrorCode.VACANCY_WRONG_NAME);
        if (vacancyName.equals("")) throw new PersonException(PersonErrorCode.VACANCY_WRONG_NAME);
        this.vacancyName = vacancyName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) throws PersonException {
        if (salary < 0) throw new PersonException(PersonErrorCode.NEGATIVE_SALARY);
        this.salary = salary;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public Requirement getRequirement(String requirementName) throws PersonException {
        if (!containsRequirementByName(requirementName)) throw new PersonException(PersonErrorCode.REQUIREMENT_NOT_FOUND);
        return getRequirementByName(requirementName);
    }

    public void addRequirement(Requirement requirement) throws PersonException {
        if (requirementList.contains(requirement)) throw new PersonException(PersonErrorCode.REQUIREMENT_DUPLICATE);
        requirementList.add(requirement);
    }

    public void removeRequirement(String requirementName) throws PersonException {
        if (!requirementList.removeIf(r -> r.getSkillName().equals(requirementName)))
            throw new PersonException(PersonErrorCode.REQUIREMENT_NOT_FOUND);
    }

    private boolean containsRequirementByName(String requirementName) {
        return requirementList.stream().filter(r -> r.getSkillName().equals(requirementName)).findFirst().isPresent();
    }

    private Requirement getRequirementByName(String requirementName) {
        return requirementList.stream().filter(r -> r.getSkillName().equals(requirementName)).findFirst().get();
    }

    public boolean getActivationState() {
        return activation;
    }

    public void setActivationState(boolean activation) {
        this.activation = activation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vacancy)) return false;

        Vacancy vacancy = (Vacancy) o;

        if (salary != vacancy.salary) return false;
        if (activation != vacancy.activation) return false;
        if (vacancyName != null ? !vacancyName.equals(vacancy.vacancyName) : vacancy.vacancyName != null) return false;
        return requirementList != null ? requirementList.equals(vacancy.requirementList) : vacancy.requirementList == null;
    }

    @Override
    public int hashCode() {
        int result = vacancyName != null ? vacancyName.hashCode() : 0;
        result = 31 * result + salary;
        result = 31 * result + (activation ? 1 : 0);
        result = 31 * result + (requirementList != null ? requirementList.hashCode() : 0);
        return result;
    }
}
