package net.some.kruglov.hiring.domain.employee;

import net.some.kruglov.hiring.activation.Activatable;
import net.some.kruglov.hiring.domain.person.Person;
import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.dto.request.RegisterEmployeeDtoRequest;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Employee extends Person implements Activatable, Serializable {
    private boolean activation;
    private Map<String, Integer> skills;

    public Employee() {
        super();
    }

    public Employee(String fullName, String email, String login, String password) {
        super(fullName, email, login, DigestUtils.md5Hex(password));
        activation = false;
        skills = new HashMap<>();
    }

    public Employee(RegisterEmployeeDtoRequest employeeDtoRequest) {
        this(employeeDtoRequest.getFullName(), employeeDtoRequest.getEmail(), employeeDtoRequest.getLogin(),
                employeeDtoRequest.getPassword());
    }

    public boolean getActivationState() {
        return activation;
    }

    public void setActivationState(boolean activation) {
        this.activation = activation;
    }

    public Map<String, Integer> getSkills() {
        return skills;
    }

    public void addSkill(String skill, int level) throws PersonException {
        if (skills.containsKey(skill)) throw new PersonException(PersonErrorCode.DUPLICATE_SKILL);
        if (level < 1 || level > 5) throw new PersonException(PersonErrorCode.SKILL_WRONG_LEVEL);
        skills.put(skill, level);
    }

    public void removeSkill(String skill) throws PersonException {
        if (!skills.containsKey(skill)) throw new PersonException(PersonErrorCode.SKILL_NOT_FOUND);
        skills.remove(skill);
    }

    public void changeSkillName(String currentSkill, String newSkill) throws PersonException {
        if (!skills.containsKey(currentSkill)) throw new PersonException(PersonErrorCode.SKILL_NOT_FOUND);
        int level = skills.get(currentSkill);
        removeSkill(currentSkill);
        addSkill(newSkill, level);
    }

    public void changeLevel(String skill, int newLevel) throws PersonException {
        if (newLevel < 1 || newLevel > 5) throw new PersonException(PersonErrorCode.SKILL_WRONG_LEVEL);
        if (!skills.containsKey(skill)) throw new PersonException(PersonErrorCode.SKILL_NOT_FOUND);
        skills.replace(skill, newLevel);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        if (!super.equals(o)) return false;
        Employee employee = (Employee) o;
        return activation == employee.activation &&
                Objects.equals(skills, employee.skills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), activation, skills);
    }
}
