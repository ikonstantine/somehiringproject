package net.some.kruglov.hiring.domain.employer;

import net.some.kruglov.hiring.domain.person.Person;
import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;
import net.some.kruglov.hiring.dto.request.RegisterEmployerDtoRequest;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Employer extends Person implements Serializable {
    private String companyName;
    private String address;
    List<Vacancy> vacancyList;

    public Employer(String fullName, String email, String login,
                    String password, String companyName, String address)  {
        super(fullName, email, login, DigestUtils.md5Hex(password));
        this.companyName = companyName;
        this.address = address;
        vacancyList = new ArrayList<>();
    }

    public Employer(RegisterEmployerDtoRequest employerDtoRequest) {
        this(employerDtoRequest.getFullName(), employerDtoRequest.getEmail(), employerDtoRequest.getLogin(),
                employerDtoRequest.getPassword(), employerDtoRequest.getCompanyName(), employerDtoRequest.getAddress());
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) throws PersonException {
        if (companyName == null) throw new PersonException(PersonErrorCode.PERSON_WRONG_COMPANY_NAME);
        if (companyName.equals("")) throw new PersonException(PersonErrorCode.PERSON_WRONG_COMPANY_NAME);
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) throws PersonException {
        if (address == null) throw new PersonException(PersonErrorCode.PERSON_WRONG_ADDRESS);
        if (address.equals("")) throw new PersonException(PersonErrorCode.PERSON_WRONG_ADDRESS);
        this.address = address;
    }

    public List<Vacancy> getVacancyList() {
        return vacancyList;
    }

    public void addVacancy(Vacancy vacancy) throws PersonException {
        if (vacancyList.contains(vacancy)) throw new PersonException(PersonErrorCode.DUPLICATE_VACANCY);
        vacancyList.add(vacancy);
    }

    public Vacancy getVacancy(String vacancyName) throws PersonException {
        if (!containsVacancyByName(vacancyName)) throw new PersonException(PersonErrorCode.VACANCY_NOT_FOUND);
        return getVacancyByName(vacancyName);
    }

    public void removeVacancy(String vacancyName) throws PersonException {
        if (!vacancyList.removeIf(v -> v.getVacancyName().equals(vacancyName)))
            throw new PersonException(PersonErrorCode.VACANCY_NOT_FOUND);
    }

    private boolean containsVacancyByName(String vacancyName) {
        return vacancyList.stream().anyMatch(v -> v.getVacancyName().equals(vacancyName));
    }

    private Vacancy getVacancyByName(String vacancyName) {
        return vacancyList.stream().filter(v -> v.getVacancyName().equals(vacancyName)).findFirst().get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employer)) return false;
        if (!super.equals(o)) return false;
        Employer employer = (Employer) o;
        return Objects.equals(companyName, employer.companyName) &&
                Objects.equals(address, employer.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), companyName, address);
    }
}
