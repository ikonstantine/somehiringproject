package net.some.kruglov.hiring.domain.person;

public enum PersonErrorCode {
    PERSON_WRONG_COMPANY_NAME(""),
    PERSON_WRONG_ADDRESS(""),
    SKILL_NOT_FOUND(""),
    DUPLICATE_SKILL(""),
    SKILL_WRONG_NAME(""),
    SKILL_WRONG_LEVEL(""),
    VACANCY_NOT_FOUND(""),
    VACANCY_WRONG_NAME(""),
    NEGATIVE_SALARY(""),
    DUPLICATE_VACANCY(""),
    REQUIREMENT_NOT_FOUND(""),
    REQUIREMENT_DUPLICATE("");

    private String errorString;

    PersonErrorCode(String errorString) {
        this.errorString = errorString;
    }
}
