package net.some.kruglov.hiring.domain.requirement;

import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;

import java.io.Serializable;

public class Requirement implements Serializable {
    private String skillName;
    private int level;
    private boolean require;

    public Requirement(String skillName, int level, boolean require) throws PersonException {
        setSkillName(skillName);
        setLevel(level);
        setRequire(require);
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) throws PersonException {
        if (skillName == null) throw new PersonException(PersonErrorCode.SKILL_WRONG_NAME);
        if (skillName.equals("")) throw new PersonException(PersonErrorCode.SKILL_WRONG_NAME);
        this.skillName = skillName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) throws PersonException {
        if (level < 1 || level > 5) throw new PersonException(PersonErrorCode.SKILL_WRONG_LEVEL);
        this.level = level;
    }

    public boolean getRequireState() {
        return require;
    }

    public void setRequire(boolean require) {
        this.require = require;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Requirement)) return false;

        Requirement that = (Requirement) o;

        if (level != that.level) return false;
        if (require != that.require) return false;
        return skillName != null ? skillName.equals(that.skillName) : that.skillName == null;
    }

    @Override
    public int hashCode() {
        int result = skillName != null ? skillName.hashCode() : 0;
        result = 31 * result + level;
        result = 31 * result + (require ? 1 : 0);
        return result;
    }
}
