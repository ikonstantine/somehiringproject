package net.some.kruglov.hiring.domain.person;

import java.io.Serializable;
import java.util.Objects;

public abstract class Person implements Serializable {
    private String email;
    private String fullName;
    private String login;
    private String hashKey;
    private String token;
    private boolean hasEntered;

    public Person() {
        super();
    }

    public Person(String fullName, String email, String login, String hashKey) {
        this.fullName = fullName;
        this.email = email;
        this.login = login;
        this.hashKey = hashKey;
        hasEntered = false;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public String getLogin() {
        return login;
    }

    public boolean getEnteredState() {
        return hasEntered;
    }

    public void switchEnteredState() {
        hasEntered = !hasEntered;
    }

    public String getToken() {
        return token.toString();
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return hasEntered == person.hasEntered &&
                Objects.equals(email, person.email) &&
                Objects.equals(fullName, person.fullName) &&
                Objects.equals(login, person.login) &&
                Objects.equals(hashKey, person.hashKey) &&
                Objects.equals(token, person.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, fullName, login, hashKey, token, hasEntered);
    }
}
