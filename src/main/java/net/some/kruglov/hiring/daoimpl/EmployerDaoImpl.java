package net.some.kruglov.hiring.daoimpl;

import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.dao.EmployerDao;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;

import java.util.List;
import java.util.UUID;

public class EmployerDaoImpl implements EmployerDao {
    private DataBase dataBase;

    public EmployerDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public Employer insert(Employer employer) throws DataBaseException {
        dataBase.addEmployer(employer);
        employer.setToken(UUID.randomUUID().toString());
        employer.switchEnteredState();
        return employer;
    }

    @Override
    public void delete(String token) {
        dataBase.removeEmployer(token);
    }

    @Override
    public void addVacancy(Vacancy vacancy, String token) throws PersonException, DataBaseException {
        vacancy.setActivationState(true);
        dataBase.addVacancy(vacancy, token);
    }

    @Override
    public Employer getEmployer(String token) throws DataBaseException {
        return dataBase.getEmployerByToken(token);
    }

    @Override
    public Vacancy getVacancy(String token, String vacancyName) throws DataBaseException, PersonException {
        return getEmployer(token).getVacancy(vacancyName);
    }

    @Override
    public List<Employee> getEmployees() {
        return dataBase.getEmployeeList();
    }

    @Override
    public Employee getEmployee(String token) throws DataBaseException {
        return dataBase.getEmployeeByToken(token);
    }
}
