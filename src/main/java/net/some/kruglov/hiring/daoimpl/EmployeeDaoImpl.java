package net.some.kruglov.hiring.daoimpl;

import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.dao.EmployeeDao;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;

import java.util.List;
import java.util.UUID;

public class EmployeeDaoImpl implements EmployeeDao {
    private DataBase dataBase;

    public EmployeeDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public Employee insert(Employee employee) throws DataBaseException {
        dataBase.addEmployee(employee);
        employee.setToken(UUID.randomUUID().toString());
        employee.switchEnteredState();
        return employee;
    }

    @Override
    public void delete(String token) {
        dataBase.removeEmployee(token);
    }

    @Override
    public Employee getEmployee(String token) throws DataBaseException {
        return dataBase.getEmployeeByToken(token);
    }

    @Override
    public List<Employer> getEmployers() {
        return dataBase.getEmployerList();
    }
}
