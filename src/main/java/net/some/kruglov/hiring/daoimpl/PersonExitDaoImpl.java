package net.some.kruglov.hiring.daoimpl;

import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.dao.PersonExitDao;

public class PersonExitDaoImpl implements PersonExitDao {
    private DataBase dataBase;

    public PersonExitDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public void exit(String token) throws DataBaseException {
        dataBase.getPersonByToken(token).switchEnteredState();
    }
}
