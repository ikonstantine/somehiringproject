package net.some.kruglov.hiring.daoimpl;

import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.dao.AuthorizationDao;
import net.some.kruglov.hiring.domain.person.Person;
import net.some.kruglov.hiring.dto.request.AuthorizationPersonDtoRequest;

public class AuthorizationDaoImpl implements AuthorizationDao {
    private DataBase dataBase;

    public AuthorizationDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public Person auth(AuthorizationPersonDtoRequest authorizationPersonDtoRequest) throws DataBaseException {
        Person person = dataBase.getPersonByLogin(authorizationPersonDtoRequest.getLogin());
        person.switchEnteredState();
        return person;
    }
}
