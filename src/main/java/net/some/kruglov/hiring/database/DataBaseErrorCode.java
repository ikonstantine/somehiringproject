package net.some.kruglov.hiring.database;

public enum DataBaseErrorCode {
    DATA_BASE_LOGIN_ALREADY_EXIST("This login already exist"),
    DATA_BASE_LOGIN_NOT_EXIST("This login not exist"),
    DATA_BASE_TOKEN_NOT_EXIST("This token not exist"),
    SKILL_NAME_DOES_EXIST("This skill's name does exist"),
    SKILLS_LIST_IS_EMPTY("Skill's list is empty");

    String errorMessage;

    DataBaseErrorCode(String errorString) {
        this.errorMessage = errorString;
    }

    @Override
    public String toString() {
        return errorMessage;
    }
}
