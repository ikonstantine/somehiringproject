package net.some.kruglov.hiring.database;

public class DataBaseException extends Exception {
    private DataBaseErrorCode code;

    public DataBaseException(DataBaseErrorCode code) {
        this.code = code;
    }

    public DataBaseErrorCode getErrorCode() {
        return code;
    }
}
