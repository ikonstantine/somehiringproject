package net.some.kruglov.hiring.database;

import net.some.kruglov.hiring.domain.person.Person;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase implements Serializable {
    private List<Employee> employeeList;
    private List<Employer> employerList;
    private List<String> skillNamesList;

    public DataBase() {
        employeeList = new ArrayList<>();
        employerList = new ArrayList<>();
        skillNamesList = new ArrayList<>();
    }

    public void addEmployee(Employee employee) throws DataBaseException {
        if (employeeList.stream().anyMatch(e -> e.getLogin().equals(employee.getLogin())) ||
                employerList.stream().anyMatch(e -> e.getLogin().equals(employee.getLogin())))
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_LOGIN_ALREADY_EXIST);
        employeeList.add(employee);
    }

    public void addEmployer(Employer employer) throws DataBaseException {
        if (isEmployeesLoginExist(employer.getLogin()) || isEmployersLoginExist(employer.getLogin()))
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_LOGIN_ALREADY_EXIST);
        employerList.add(employer);
    }

    public void removeEmployee(String token) {
        employeeList.removeIf(e -> e.getToken().equals(token));
    }

    public void removeEmployer(String token) {
        employerList.removeIf(e -> e.getToken().equals(token));
    }

    public Person getPersonByLogin(String login) throws DataBaseException {
        if (employeeList.stream().anyMatch(e -> e.getLogin().equals(login)))
            return employeeList.stream().filter(e -> e.getLogin().equals(login)).findFirst().get();
        else if (employerList.stream().anyMatch(e -> e.getLogin().equals(login)))
            return employerList.stream().filter(e -> e.getLogin().equals(login)).findFirst().get();
        else
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_LOGIN_NOT_EXIST);
    }

    public Person getPersonByToken(String token) throws DataBaseException {
        if (employeeList.stream().anyMatch(e -> e.getToken().equals(token)))
            return employeeList.stream().filter(e -> e.getToken().equals(token)).findFirst().get();
        else if (employerList.stream().anyMatch(e -> e.getToken().equals(token)))
            return employerList.stream().filter(e -> e.getToken().equals(token)).findFirst().get();
        else
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_TOKEN_NOT_EXIST);
    }

    public Employer getEmployerByToken(String token) throws DataBaseException {
        if (employerList.stream().anyMatch(e -> e.getToken().equals(token)))
            return employerList.stream().filter(e -> e.getToken().equals(token)).findFirst().get();
        else
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_TOKEN_NOT_EXIST);
    }

    public Employee getEmployeeByToken(String token) throws DataBaseException {
        if (employeeList.stream().anyMatch(e -> e.getToken().equals(token)))
            return employeeList.stream().filter(e -> e.getToken().equals(token)).findFirst().get();
        else
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_TOKEN_NOT_EXIST);
    }

    public void addVacancy(Vacancy vacancy, String token) throws PersonException, DataBaseException {
        if (employerList.stream().anyMatch(e -> e.getToken().equals(token)))
            employerList.stream().filter(e -> e.getToken().equals(token)).findFirst().get().addVacancy(vacancy);
        else
            throw new DataBaseException(DataBaseErrorCode.DATA_BASE_TOKEN_NOT_EXIST);
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public List<Employer> getEmployerList() {
        return employerList;
    }

    public List<String> getSkillNamesList() throws DataBaseException {
        if (skillNamesList.isEmpty()) throw new DataBaseException(DataBaseErrorCode.SKILLS_LIST_IS_EMPTY);
        return skillNamesList;
    }

    public void addSkillName(String skillName) throws DataBaseException {
        if (skillNamesList.stream().anyMatch(s -> s.toLowerCase().equals(skillName.toLowerCase())))
            throw new DataBaseException(DataBaseErrorCode.SKILL_NAME_DOES_EXIST);
        skillNamesList.add(skillName);
    }

    private boolean isEmployeesLoginExist(String login) {
        return employeeList.stream().anyMatch(e -> e.getLogin().equals(login));
    }

    private boolean isEmployersLoginExist(String login) {
        return employerList.stream().anyMatch(e -> e.getLogin().equals(login));
    }
}
