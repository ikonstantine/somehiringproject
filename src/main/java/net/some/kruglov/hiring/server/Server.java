package net.some.kruglov.hiring.server;

import net.some.kruglov.hiring.database.DataBase;
import net.some.kruglov.hiring.service.*;

import java.io.*;

public class Server {
    private EmployerService employerService;
    private EmployeeService employeeService;
    private AuthorizationService authorizationService;
    private PersonOutService personOutService;
    private SkillsListService skillTitleService;
    private DataBase dataBase;
    private boolean running = false;

    public void start() {
        if (dataBase == null) dataBase = new DataBase();
        employeeService = new EmployeeService(dataBase);
        employerService = new EmployerService(dataBase);
        authorizationService = new AuthorizationService(dataBase);
        personOutService = new PersonOutService(dataBase);
        skillTitleService = new SkillsListService(dataBase);
        running = true;
    }

    public void start(String fileName) {
        loadDataBase(fileName);
        start();
    }

    public void stop(String fileName) {
        saveDataBase(fileName);
        running = false;
    }

    public void loadDataBase(String fileName) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            dataBase = (DataBase) ois.readObject();
        } catch (ClassNotFoundException | IOException exception) {
            System.err.print(exception.getMessage());
        }
    }

    public void saveDataBase(String fileName) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(dataBase);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isRunning() {
        return running;
    }

    public String registerEmployee(String jsonRequest) {
        return employeeService.registerEmployee(jsonRequest);
    }

    public String registerEmployer(String jsonRequest) {
        return employerService.registerEmployer(jsonRequest);
    }

    public String authorization(String jsonRequest) {
        return authorizationService.authorizationPerson(jsonRequest);
    }

    public String exitFromServer(String jsonRequest) {
        return personOutService.exitFromServer(jsonRequest);
    }

    public String leaveFromServerByEmployee(String jsonRequest) {
        return employeeService.leaveEmployee(jsonRequest);
    }

    public String leaveFromServerByEmployer(String jsonRequest) {
        return employerService.leaveEmployer(jsonRequest);
    }

    public String addVacancy(String jsonRequest) {
        return employerService.addVacancy(jsonRequest);
    }

    public String editEmployerData(String jsonRequest) {
        return employerService.editEmployerData(jsonRequest);
    }

    public String editVacancyData(String jsonRequest) {
        return employerService.editVacancyData(jsonRequest);
    }

    public String addSkills(String jsonRequest) {
        return employeeService.addSkills(jsonRequest);
    }

    public String editEmployeeData(String jsonRequest) {
        return employeeService.editEmployeeData(jsonRequest);
    }

    public String getEmployeesForEachVacancy(String jsonRequest) {
        return employerService.getEmployeesForEachVacancy(jsonRequest);
    }

    public String getEmployeesForEachVacancyForNecessaryRequirements(String jsonRequest) {
        return employerService.getEmployeesForEachVacancyForNecessaryRequirement(jsonRequest);
    }

    public String getEmployeesForEachVacancyForNecessaryRequirementWithOutLevel(String jsonRequest) {
        return employerService.getEmployeesForEachVacancyForNecessaryRequirementWithOutLevel(jsonRequest);
    }

    public String getEmployeesForEachVacancyAtLeastOneRequirement(String jsonRequest) {
        return employerService.getEmployeesForEachVacancyAtLeastOneRequirement(jsonRequest);
    }

    public String getVacanciesForEmployeeAllMatchingRequirements(String jsonRequest) {
        return employeeService.getVacanciesForEmployeeAllMatchingRequirements(jsonRequest);
    }

    public String getVacanciesForEmployeeNecessaryRequirement(String jsonRequest) {
        return employeeService.getVacanciesForEmployeeNecessaryRequirement(jsonRequest);
    }

    public String getVacanciesForEmployeeAllMatchingRequirementsWithOutLevel(String jsonRequest) {
        return employeeService.getVacanciesForEmployeeAllMatchingRequirementsWithOutLevel(jsonRequest);
    }

    public String getVacanciesForEmployeeAtLeastOneRequirement(String jsonRequest) {
        return employeeService.getVacanciesForEmployeeAtLeastOneRequirement(jsonRequest);
    }

    public String acceptEmployee(String jsonRequest) {
        return employerService.acceptEmployee(jsonRequest);
    }

    public String addSkillToSkillsList(String jsonRequest) {
        return skillTitleService.addSkill(jsonRequest);
    }

    public String getSkillsList() {
        return skillTitleService.getSkillTitles();
    }

}
