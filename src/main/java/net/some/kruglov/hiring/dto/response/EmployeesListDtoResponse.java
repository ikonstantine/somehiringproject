package net.some.kruglov.hiring.dto.response;

import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeesListDtoResponse {
    private Map<String, List<PersonDto>> vacancyList;

    public EmployeesListDtoResponse() {
        vacancyList = new HashMap<>();
    }

    public void putVacancy(String vacancyName, List<PersonDto> employeesList) {
        vacancyList.put(vacancyName, employeesList);
    }

    public String jsonResponse() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
