package net.some.kruglov.hiring.dto.response;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class VacanciesListDtoResponse {
    private List<MatchingVacancyDto> list;

    public VacanciesListDtoResponse() {
        list = new ArrayList<>();

    }

    public List<MatchingVacancyDto> getList() {
        return list;
    }

    public void putVacancy(MatchingVacancyDto vacancyDto) {
        list.add(vacancyDto);
    }

    public String jsonResponse() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
