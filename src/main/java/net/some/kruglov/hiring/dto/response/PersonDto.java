package net.some.kruglov.hiring.dto.response;

public class PersonDto {
    private String name;
    private String email;
    private String token;

    public PersonDto(String name, String email, String token) {
        setName(name);
        setEmail(email);
        setToken(token);
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
