package net.some.kruglov.hiring.dto.request;

import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;

public class GetVacanciesForEmployeeDtoRequest {
        private String token;

        public GetVacanciesForEmployeeDtoRequest(String token) {
            setToken(token);
        }

        public void validate() throws ValidatorException {
            Validator.validateToken(token);
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
}
