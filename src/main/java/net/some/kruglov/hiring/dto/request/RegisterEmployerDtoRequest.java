package net.some.kruglov.hiring.dto.request;

import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;

public class RegisterEmployerDtoRequest {
    private String fullName;
    private String email;
    private String login;
    private String password;
    private String companyName;
    private String address;

    public RegisterEmployerDtoRequest(String fullName, String email, String login, String password, String companyName,
                                      String address) {
        this.fullName = fullName;
        this.email = email;
        this.login = login;
        this.password = password;
        this.companyName = companyName;
        this.address = address;
    }

    public void validate() throws ValidatorException {
        Validator.validateName(fullName);
        Validator.validateEmail(email);
        Validator.validateLogin(login);
        Validator.validatePassword(password);
        Validator.validateName(companyName);
        Validator.validateName(address);
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
