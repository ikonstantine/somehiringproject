package net.some.kruglov.hiring.dto.response;

import com.google.gson.JsonObject;

public class ErrorDtoResponse {
    public static String jsonResponse(String message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("error", message);
        return jsonObject.toString();
    }
}
