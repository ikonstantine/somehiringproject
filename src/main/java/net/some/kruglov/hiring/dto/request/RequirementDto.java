package net.some.kruglov.hiring.dto.request;

public class RequirementDto {
    private String skillName;
    private Integer level;
    private Boolean require;

    public RequirementDto(String skillName, Integer level, Boolean require) {
        setSkillName(skillName);
        setLevel(level);
        setRequire(require);
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level)  {
        this.level = level;
    }

    public Boolean getRequireState() {
        return require;
    }

    public void setRequire(Boolean require) {
        this.require = require;
    }
}
