package net.some.kruglov.hiring.dto.response;


import com.google.gson.GsonBuilder;

import java.util.List;

public class SkillTitlesListDtoResponse {
    private List<String> skillTitlesList;

    public SkillTitlesListDtoResponse(List<String> list) {
        setSkillTitlesList(list);
    }

    public List<String> getSkillTitlesList() {
        return skillTitlesList;
    }

    public void setSkillTitlesList(List<String> skillTitlesList) {
        this.skillTitlesList = skillTitlesList;
    }

    public String jsonResponse() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
