package net.some.kruglov.hiring.dto.request;

import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorErrorCode;
import net.some.kruglov.hiring.validator.ValidatorException;

public class ChangeEmployerDataDtoRequest {
    private String token;
    private String fullName;
    private String email;
    private String password;
    private String companyName;
    private String address;

    public ChangeEmployerDataDtoRequest(String token, String fullName, String email, String password, String companyName,
                                        String address) {
        setFullName(fullName);
        setEmail(email);
        setPassword(password);
        setCompanyName(companyName);
        setAddress(address);
        setToken(token);
    }

    public void validate() throws ValidatorException {
        Validator.validateToken(token);
        final int NUMBER_OF_FIELDS = 5;
        int counter = 0;
        if (fullName == null) counter++;
        if (email == null) counter++;
        if (password == null) counter++;
        if (companyName == null) counter++;
        if (address == null) counter++;
        if (counter == NUMBER_OF_FIELDS) throw new ValidatorException(ValidatorErrorCode.EMPLOYER_NOTHING_TO_EDIT);
    }

    public String getToken() {
        return token;
    }

    public String getAddress() {
        return address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
