package net.some.kruglov.hiring.dto.request;

import net.some.kruglov.hiring.domain.person.PersonErrorCode;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.requirement.Requirement;

import java.util.List;

public class VacancyDtoRequest {
    private String token;
    private String nameVacancy;
    private int salary;
    private List<Requirement> requirementList;

    public VacancyDtoRequest(String token, String nameVacancy, int salary, List<Requirement> requirementList) {
        this.token = token;
        this.nameVacancy = nameVacancy;
        this.salary = salary;
        this.requirementList = requirementList;
    }

    public void validate() throws PersonException {
        for (Requirement item : requirementList) {
            if (item.getSkillName() == null) throw new PersonException(PersonErrorCode.SKILL_WRONG_NAME);
            if (item.getSkillName().equals("")) throw new PersonException(PersonErrorCode.SKILL_WRONG_NAME);
            if (item.getLevel() < 1 || item.getLevel() > 5) throw new PersonException(PersonErrorCode.SKILL_WRONG_LEVEL);
        }
    }

    public String getToken() {
        return token;
    }

    public int getSalary() {
        return salary;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public String getNameVacancy() {
        return nameVacancy;
    }

    public void setNameVacancy(String nameVacancy) {
        this.nameVacancy = nameVacancy;
    }

    public void setRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setToken(String token) {
        this.token = token;
    }
}


