package net.some.kruglov.hiring.dto.request;

public class ExitPersonDtoRequest {
    private String token;
    private boolean doExit;

    public ExitPersonDtoRequest(String token, boolean doExit) {
        setToken(token);
        setDoExit(doExit);
    }

    public String getToken() {
        return token;
    }

    public boolean isDoExit() {
        return doExit;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setDoExit(boolean doExit) {
        this.doExit = doExit;
    }
}
