package net.some.kruglov.hiring.dto.response;

import com.google.gson.JsonObject;

public class TokenDtoResponse {
    public static String jsonResponse(String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("token", token);
        return jsonObject.toString();
    }
}
