package net.some.kruglov.hiring.dto.request;

public class AuthorizationPersonDtoRequest {
    private String login;
    private String password;

    public AuthorizationPersonDtoRequest(String login, String password) {
        setLogin(login);
        setPassword(password);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
