package net.some.kruglov.hiring.dto.request;

public class AcceptEmployeeDtoRequest {
    private String employerToken;
    private String vacancyName;
    private String employeeToken;

    public AcceptEmployeeDtoRequest(String vacancyName, String employerToken, String employeeToken) {
        setVacancyName(vacancyName);
        setEmployeeToken(employeeToken);
        setEmployerToken(employerToken);
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public String getEmployeeToken() {
        return employeeToken;
    }

    public String getEmployerToken() {
        return employerToken;
    }

    public void setVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
    }

    public void setEmployeeToken(String employeeToken) {
        this.employeeToken = employeeToken;
    }

    public void setEmployerToken(String employerToken) {
        this.employerToken = employerToken;
    }
}
