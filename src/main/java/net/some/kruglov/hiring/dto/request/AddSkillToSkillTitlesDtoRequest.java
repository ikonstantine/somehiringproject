package net.some.kruglov.hiring.dto.request;

public class AddSkillToSkillTitlesDtoRequest {
    private String skill;

    public AddSkillToSkillTitlesDtoRequest(String skill) {
        setSkill(skill);
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}
