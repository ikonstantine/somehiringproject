package net.some.kruglov.hiring.dto.request;

import java.util.List;

public class SkillsDtoRequest {
    private String token;
    private List<RequirementDto> skills;

    public SkillsDtoRequest(String token, List<RequirementDto> skills) {
        setToken(token);
        setSkills(skills);
    }

    public String getToken() {
        return token;
    }

    public List<RequirementDto> getSkills() {
        return skills;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setSkills(List<RequirementDto> skills) {
        this.skills = skills;
    }
}
