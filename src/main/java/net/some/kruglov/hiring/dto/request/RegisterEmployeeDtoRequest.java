package net.some.kruglov.hiring.dto.request;

import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;

public class RegisterEmployeeDtoRequest {
    private String fullName;
    private String login;
    private String password;
    private String email;

    public RegisterEmployeeDtoRequest(String fullName, String email, String login, String password) {
        this.fullName = fullName;
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public void validate() throws ValidatorException {
        Validator.validateName(fullName);
        Validator.validateEmail(email);
        Validator.validateLogin(login);
        Validator.validatePassword(password);
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
