package net.some.kruglov.hiring.dto.request;

import net.some.kruglov.hiring.validator.Validator;
import net.some.kruglov.hiring.validator.ValidatorException;

import java.util.Map;

public class ChangeEmployeeDataDtoRequest {
    private String token;
    private String fullName;
    private String email;
    private String password;
    private Boolean activate;
    Map<String, RequirementDto> skills;

    public ChangeEmployeeDataDtoRequest(String token, String fullName, String email, String password, Boolean activate,
                                        Map<String, RequirementDto> skills) {
        setToken(token);
        setFullName(fullName);
        setEmail(email);
        setPassword(password);
        setActivate(activate);
        setSkills(skills);
    }

    public void validate() throws ValidatorException {
        Validator.validateToken(token);
        if (password != null) Validator.validatePassword(password);
        if (email != null) Validator.validateEmail(email);
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public Boolean getActivate() {
        return activate;
    }

    public Map<String, RequirementDto> getSkills() {
        return skills;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setActivate(Boolean activate) {
        this.activate = activate;
    }

    public void setSkills(Map<String, RequirementDto> skills) {
        this.skills = skills;
    }
}

