package net.some.kruglov.hiring.dto.request;

import java.util.Map;

public class ChangeVacancyDtoRequest {
    private String token;
    private String currentVacancyName;
    private String newVacancyName;
    private Integer newSalary;
    private Boolean activantion;
    private Map<String, RequirementDto> requirementMap;

    public ChangeVacancyDtoRequest(String token, String currentVacancyName, String newVacancyName, Integer newSalary,
                                   Boolean activantion, Map<String, RequirementDto> requirementMap) {
        setToken(token);
        setCurrentVacancyName(currentVacancyName);
        setNewVacancyName(newVacancyName);
        setNewSalary(newSalary);
        setRequirementMap(requirementMap);
    }

    public String getToken() {
        return token;
    }

    public String getCurrentVacancyName() {
        return currentVacancyName;
    }

    public String getNewVacancyName() {
        return newVacancyName;
    }

    public Integer getNewSalary() {
        return newSalary;
    }

    public Boolean getActivantion() {
        return activantion;
    }

    public Map<String, RequirementDto> getRequirementMap() {
        return requirementMap;
    }

    public void setActivantion(Boolean activantion) {
        this.activantion = activantion;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setCurrentVacancyName(String currentVacancyName) {
        this.currentVacancyName = currentVacancyName;
    }

    public void setNewVacancyName(String newVacancyName) {
        this.newVacancyName = newVacancyName;
    }

    public void setNewSalary(Integer newSalary) {
        this.newSalary = newSalary;
    }

    public void setRequirementMap(Map<String, RequirementDto> requirementMap) {
        this.requirementMap = requirementMap;
    }
}
