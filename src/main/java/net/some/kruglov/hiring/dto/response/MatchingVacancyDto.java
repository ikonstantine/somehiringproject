package net.some.kruglov.hiring.dto.response;

public class MatchingVacancyDto {
    private String vacancyName;
    private String email;
    private String companyName;
    private Integer matchingCount;

    public MatchingVacancyDto(String vacancyName, String email, String companyName, Integer matchingCount) {
        setVacancyName(vacancyName);
        setEmail(email);
        setCompanyName(companyName);
        setMatchingCount(matchingCount);
    }

    public MatchingVacancyDto(String vacancyName, String email, String companyName) {
        setVacancyName(vacancyName);
        setEmail(email);
        setCompanyName(companyName);
    }

    public String getEmail() {
        return email;
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Integer getMatchingCount() {
        return matchingCount;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setMatchingCount(Integer matchingCount) {
        this.matchingCount = matchingCount;
    }
}
