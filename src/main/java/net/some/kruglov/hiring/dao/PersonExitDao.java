package net.some.kruglov.hiring.dao;

import net.some.kruglov.hiring.database.DataBaseException;

public interface PersonExitDao {
    void exit(String token) throws DataBaseException;
}
