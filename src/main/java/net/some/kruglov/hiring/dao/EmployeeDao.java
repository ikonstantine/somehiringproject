package net.some.kruglov.hiring.dao;

import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;

import java.util.List;

public interface EmployeeDao {
    Employee insert(Employee employee) throws DataBaseException;
    void delete(String token);
    Employee getEmployee(String token) throws DataBaseException;
    List<Employer> getEmployers();
}
