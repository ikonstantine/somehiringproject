package net.some.kruglov.hiring.dao;

import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.domain.person.Person;
import net.some.kruglov.hiring.dto.request.AuthorizationPersonDtoRequest;

public interface AuthorizationDao {
    Person auth(AuthorizationPersonDtoRequest authorizationPersonDtoRequest) throws DataBaseException;
}
