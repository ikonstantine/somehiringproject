package net.some.kruglov.hiring.dao;

import net.some.kruglov.hiring.database.DataBaseException;
import net.some.kruglov.hiring.domain.employee.Employee;
import net.some.kruglov.hiring.domain.employer.Employer;
import net.some.kruglov.hiring.domain.person.PersonException;
import net.some.kruglov.hiring.domain.vacancy.Vacancy;

import java.util.List;

public interface EmployerDao {
    Employer insert(Employer employer) throws DataBaseException;
    void delete(String token) throws DataBaseException;
    void addVacancy(Vacancy vacancy, String token) throws PersonException, DataBaseException;
    Employer getEmployer(String token) throws DataBaseException;
    Vacancy getVacancy(String token, String vacancyName) throws DataBaseException, PersonException;
    List<Employee> getEmployees();
    Employee getEmployee(String token) throws DataBaseException;
}
