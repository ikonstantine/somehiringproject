package net.some.kruglov.hiring.validator;

import java.util.regex.Pattern;

public class Validator {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static final Pattern VALID_PASSWORD_REGEX =
            Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", Pattern.CASE_INSENSITIVE);

    public static void validateEmail(String email) throws ValidatorException {
        if (email == null) throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_EMAIL);
        if (email.equals("") || !VALID_EMAIL_ADDRESS_REGEX.matcher(email).find())
            throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_EMAIL);
    }

    public static void validatePassword(String password) throws ValidatorException {
        if (password == null) throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_PASSWORD);
        if (password.equals("")) throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_PASSWORD);
        if (!VALID_PASSWORD_REGEX.matcher(password).find())
            throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_PASSWORD);
    }

    public static void validateName(String fullName) throws ValidatorException {
        if (fullName == null) throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_NAME);
        if (fullName.equals("") || fullName.length() <= 1) throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_NAME);
    }

    public static void validateLogin(String login) throws ValidatorException {
        if (login == null) throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_LOGIN);
        if (login.equals("") || login.length() < 5)
            throw new ValidatorException(ValidatorErrorCode.NOT_VALIDATE_LOGIN);
    }

    public static void equalsHashKeys(String hashKey1, String hashKey2) throws ValidatorException {
        if (!hashKey1.equals(hashKey2)) throw new ValidatorException(ValidatorErrorCode.WRONG_PASSWORD);
    }

    public static void validateToken(String token) throws ValidatorException {
        if (token == null) throw new ValidatorException(ValidatorErrorCode.TOKEN_IS_EMPTY);
        if (token.equals("")) throw new ValidatorException(ValidatorErrorCode.TOKEN_IS_EMPTY);
    }
}
