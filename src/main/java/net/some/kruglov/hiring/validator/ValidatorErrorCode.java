package net.some.kruglov.hiring.validator;

public enum ValidatorErrorCode {

    NOT_VALIDATE_PASSWORD("Password mustn't be least 8 chars. Doesn't contain space, tab. Contains one digit, one special char," +
            " one lower and upper char"),
    NOT_VALIDATE_EMAIL("Email should consist of a name and a domain name separated by \'@\'. Example: \'name@domen_name.com\'"),
    NOT_VALIDATE_NAME("Name mustn't be null or empty string and least 2 chars"),
    NOT_VALIDATE_LOGIN("Login mustn't be null or empty string and least 5 chars"),
    TOKEN_IS_EMPTY("Token is empty"),
    WRONG_PASSWORD("Wrong password"),
    EMPLOYER_NOTHING_TO_EDIT("Nothing to edit");

    public String errorString;

    ValidatorErrorCode(String errorString) {
        this.errorString = errorString;
    }

    @Override
    public String toString() {
        return errorString;
    }
}
