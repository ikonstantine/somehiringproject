package net.some.kruglov.hiring.validator;

public class ValidatorException extends Exception {
    private ValidatorErrorCode code;

    public ValidatorException(ValidatorErrorCode code) {
        this.code = code;
    }

    public ValidatorErrorCode getErrorCode() {
        return code;
    }
}
