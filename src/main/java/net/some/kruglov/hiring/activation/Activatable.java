package net.some.kruglov.hiring.activation;

public interface Activatable {
    public boolean getActivationState();
    public void setActivationState(boolean activateion);
}
